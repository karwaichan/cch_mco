<?php

	namespace Tests;

	use App\TestingObjects\ORT\StatusByDocId as ORTStatusByDocId;
	use App\TestingObjects\ORT\StatusByPeriod as ORTStatusByPeriod;
	use App\TestingObjects\ORT\VoidTransactions as ORTVoidTransactions;
	use App\TestingObjects\UAT\IssueTransactions;
	use App\TestingObjects\ORT\IssueTransactions as ORTIssueTransactions;
	use App\TestingObjects\UAT\StatusByDocId;
	use App\TestingObjects\UAT\StatusByPeriod;
	use Illuminate\Support\Carbon;

	class ApiTest extends TestCase
	{
		protected $fake;
		protected $ORTissueTransaction;
		protected $UATissueTransaction;
		protected $UATstatusByPeriod;
		protected $ORTstatusByDocId;
		protected $ORTstatusByPeriod;
		protected $UATstatusByDocId;
		protected $ORTvoidTransaction;

		public function __construct()
		{
			parent::__construct();
			$this->ORTissueTransaction = new ORTIssueTransactions();
			$this->ORTstatusByPeriod   = new ORTStatusByPeriod();
			$this->ORTstatusByDocId    = new ORTStatusByDocId();
			$this->ORTvoidTransaction  = new ORTVoidTransactions();
			$this->UATissueTransaction = new IssueTransactions();
			$this->UATstatusByPeriod   = new StatusByPeriod();
			$this->UATstatusByDocId    = new StatusByDocId();
		}

		/**
		 * @param $input
		 * @param $output
		 * @param $fileName
		 */
		protected function saveToFile($input, $output, $fileName): void
		{
			if (file_exists(\dirname(__DIR__, 1) . '/storage/testing/' . $fileName . '_' . Carbon::now()->toDateString() . '.txt') === false) {
				$data = json_encode([array_merge($input, json_decode($output, true))]);
			}
			else {
				$files       = json_decode(file_get_contents(\dirname(__DIR__, 1) . '/storage/testing/' . $fileName . '_' . Carbon::now()->toDateString() . '.txt'), true);
				$currentData = array_merge($input, json_decode($output, true));
				array_push($files, $currentData);
				$data = json_encode($files);
			}

			file_put_contents(\dirname(__DIR__, 1) . '/storage/testing/' . $fileName . '_' . Carbon::now()->toDateString() . '.txt', $data);
		}

	}

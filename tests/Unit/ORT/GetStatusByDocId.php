<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 25/2/2019
	 * Time: 5:09 PM
	 */

	namespace Tests\Unit\ORT;


	use Illuminate\Foundation\Testing\TestResponse;
	use Tests\ApiTest;

	class GetStatusByDocId extends ApiTest
	{
		/**
		 * @param $input
		 * @param $fileName
		 * @return TestResponse
		 */
		private function postToStatusByDocId($input, $fileName): TestResponse
		{
			$response = $this->json('POST', '/api/statusById', $input);

			var_dump(json_encode($input));
			var_dump($response->getContent());

			$this->saveToFile($input, $response->getContent(), $fileName);

			return $response;
		}

		/**
		 *Case 14:
		 *
		 * "1. In ""Get Transaction Status (Batch) By Doc Id"", following values:
		 * Doc ID: <valid Doc ID that does not exist in UAT DB>
		 *
		 * 2. Click Retrieve Button"
		 *
		 * "CCH system returns the result, with 0 transactions.
		 *
		 * ""responseCode"": 1001
		 * ""responseMessage"": ""Transactions not found in db.""
		 *
		 * "
		 * @test
		 */
		public function success_transaction_not_in_database(): void
		{
			$input = $this->ORTstatusByDocId->make_transaction_not_in_database();

			$response = $this->postToStatusByDocId($input, 'Case14');

			$response->assertStatus(200);
		}

		/**
		 * Case 15:
		 *
		 * "1. In ""Get Transaction Status (Batch) By Doc Id"", following values:
		 * Doc ID: <valid Doc ID that exists in UAT DB>
		 *
		 * 2. Click Retrieve Button"
		 * Result:
		 * "CCH system returns the transaction matching the Doc ID.
		 *
		 * Each transaction in the results consist of either below fields:
		 * 1. Issued Status Update Event:
		 * CRA/IR ID, Doc ID, Issued Status, Transaction Update Date/Time
		 * 2. Customs Status Update Event:
		 * CRA/IR ID, Doc ID, Custom Status, Custom Stamp Date/Time, Transaction Update Date/Time, Exported Transaction Gross Amount, Refund Amount
		 * 3. Refund Status Update Event:
		 * CRA/IR ID, Doc ID, Refund Status, Refund Date/Time, Refund Method, Refund Amount, CRC or DTRO ID, Transaction Update Date/Time
		 * "
		 * @test
		 */
		public function success_retrieve_transaction(): void
		{
			$input = $this->ORTstatusByDocId->make_retrieve_transaction();

			$response = $this->postToStatusByDocId($input, 'Case15');

			$response->assertStatus(200);
		}
	}
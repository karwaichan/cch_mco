<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 27/2/2019
	 * Time: 1:33 PM
	 */

	namespace Tests\Unit\ORT;


	use Illuminate\Foundation\Testing\TestResponse;
	use Tests\ApiTest;

	/**
	 * Please get the doc_id for separate environment before proceeding with VOIDING testing
	 *
	 * Class VoidTransactionApiTest
	 * @package Tests\Unit\ORT
	 */
	class VoidTransactionApiTest extends ApiTest
	{
		/**
		 * @param array $input
		 * @param string $fileName
		 * @return TestResponse
		 */
		private function postToApi(array $input, string $fileName): TestResponse
		{
			$response = $this->json('POST', '/api/void', $input);
			var_dump(json_encode($input));
			var_dump($response->getContent());
			$this->saveToFile($input, $response->getContent(), $fileName);

			return $response;
		}

		/**
		 * Case 6:
		 *
		 * "1) CRA/IR to send the message to void the transaction.
		 *
		 * 2) Check that all the mandatory fields required are present.
		 *
		 * 3) Check that the transaction to be updated complies with the pre-conditions stated.
		 *
		 * 4) Update the transaction as retailer voided."
		 *
		 * Result:
		 * "The transaction is successfully updated as retailer voided.
		 *
		 * ""responseCode"":""0000"",
		 * ""responseDescription"":""void  successful"""
		 *
		 * @test
		 */
		public function success_void_transaction(): void
		{
			$input = $this->ORTvoidTransaction->make_success_void_transaction();

			$response = $this->postToApi($input, 'Case6');

			$response->assertStatus(200);
		}

		/**
		 * Case 7 :
		 *
		 * CRA/IR to send the message to void the transaction where the transaction does not exist in CCH DB.
		 *
		 * Result:
		 * "Voiding by retailer not allowed. CCH to return the specific error message.
		 * ""responseCode"":""2000"",
		 * ""responseDescription"":""Transaction not found"""
		 * @test
		 */
		public function fail_void_transaction_not_in_database(): void
		{
			$input = $this->ORTvoidTransaction->make_fail_void_transaction_not_in_database();

			$response = $this->postToApi($input, 'Case7');

			$response->assertStatus(200);
		}

		/**
		 * Case 9:
		 *
		 * "1. In ""Update (Void) Transaction"", enter the following values:
		 * Doc ID: <valid Doc ID that exists in UAT DB and has Custom status = Approved>
		 *
		 * (Use the passport number/Nationality used in Issue Transaction above).
		 *
		 * 2. Click Submit Button"
		 *
		 * Result :
		 *
		 * "CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due Transaction has Custom status = Approved/Rejected)
		 *
		 * ResponseCode: 2030
		 * ResponseMessage: ""Transaction cannot be updated as it was already processed by customs."""
		 * @test
		 */
		public function fail_void_approved_transaction(): void
		{
			$input = $this->ORTvoidTransaction->make_fail_void_approved_transaction();

			$response = $this->postToApi($input, 'Case9');

			$response->assertStatus(200);
		}


	}
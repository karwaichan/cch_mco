<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 27/2/2019
	 * Time: 1:17 PM
	 */

	namespace Tests\Unit\ORT;


	use Tests\ApiTest;

	class GetBatchStatusApiTest extends ApiTest
	{
		/**
		 * "CRA/IR to send request for getstatus from CCH."
		 * "Retrieve the latest updates of all CRA/IR-issued Transactions (e.g. Issued / Customs / Refund Status).
		 *
		 * Results:
		 * Key fields for Issued Status Update Event:
		 * • CRA/IR ID
		 * • Doc-ID
		 * • Issued Status
		 * • Transaction Update Date / Time
		 *
		 * Key fields for Customs Status Update Event:
		 * • CRA/IR ID
		 * • Doc-ID
		 * • Customs Status
		 * • Customs Stamp Date / Time
		 * • Transaction Update Date / Time
		 * • Exported Transaction Gross Amount
		 * • Refund Amount
		 *
		 * Key fields for Refund Status Update Event:
		 * • CRA/IR ID
		 * • Doc-ID
		 * • Refund Status
		 * • Refund Date / Time
		 * • Refund Method
		 * • Refund Amount
		 * • CRC or DTRO ID
		 * • Transaction Update Date / Time"
		 *
		 * @test
		 */
		public function success_get_all_batch_status(): void
		{
			$response = $this->json('GET', '/api/batchStatus', []);

			var_dump($response->getContent());

			$this->saveToFile([], $response->getContent(), 'Case11');

			$response->assertStatus(200);
		}
	}
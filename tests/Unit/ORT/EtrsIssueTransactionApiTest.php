<?php

	use Carbon\Carbon;
	use Illuminate\Foundation\Testing\TestResponse;
	use Tests\ApiTest;

	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 25/2/2019
	 * Time: 1:45 PM
	 */
	class EtrsIssueTransactionApiTest extends ApiTest
	{

		/**
		 * Case 1:
		 *
		 *"Create a transaction with 4 receipts tagged:
		 *
		 * 1) Receipt date: 07/09/2018, amount = $20
		 * 2) Receipt date: 07/09/2018, amount = $25
		 * 3) Receipt date: 07/09/2018, amount = $40
		 * 4) Receipt date: 07/09/2018, amount = $15
		 *
		 * Validate the transaction."
		 *
		 * Result:
		 * "Receipts 1-4 cannot be consolidated to issue a transaction as it takes 4 receipts to form a combined total of >=$100.
		 *
		 * Therefore, this eTRS transaction is invalid and shouldn’t be approved even if it is present in the system."
		 *
		 * @test
		 */
		public function failed_consolidate_4_receipts_equal_100(): void
		{
			$input = $this->ORTissueTransaction->make_failed_consolidate_4_receipts_equal_100();

			$response = $this->postToApi($input, 'Case1');

			$response->assertStatus(200);
		}

		/**
		 * Case 2:
		 *
		 *"Create a transaction with 4 receipts tagged:
		 *
		 * 1) Receipt date: 07/09/2018, amount = $15
		 * 2) Receipt date: 07/09/2018, amount = $10
		 * 3) Receipt date: 07/09/2018, amount = $80
		 * 4) Receipt date: 08/09/2018, amount = $20
		 *
		 * Validate the transaction."
		 * RESULT:
		 * "Receipts 1-3 can be consolidated to issue a transaction as it takes 3 receipts to form a combined total of >=$100.
		 *
		 * Receipt 4 cannot be consolidated with the other 3 receipts to issue a transaction as itself is <$100 and is from a different receipt date.
		 *
		 * Therefore, this transaction is only valid based on the first 3 receipts."
		 * @test
		 */
		public function success_3_out_of_4_receipts_more_than_100(): void
		{
			$input = $this->ORTissueTransaction->make_3_out_of_4_receipts_more_than_100();

			$response = $this->postToApi($input, 'Case2');

			$response->assertStatus(200);
		}

		/**
		 * "Create 1 transaction with all the mandatory fields included:
		 *
		 * Mandatory fields include (20 fields):
		 * 1) Doc ID
		 * 2) Refund Rules
		 * 3) Shop ID
		 * 4) Shop Name 1
		 * 5) Shop Postal Code
		 * 6) Retailer GST Registration Number
		 * 7) Issuing Solution Activation Date
		 * 8) Tourist Passport Number
		 * 9) Tourist Nationality
		 * 10) Issued Date Time
		 * 11) Provisional Refund Amount
		 * 12) Service Fee Amount
		 * 13) Receipt Number
		 * 14) Receipt Date/Time
		 * 15) GST Rate
		 * 16) Purchase Item Gross Amount
		 * 17) Purchase Item Standard Goods Category"
		 *
		 * Results:
		 * The transaction with all the mandatory fields entered will be imported successfully.
		 * @test
		 */
		public function success_with_all_mandatory_fields(): void
		{
			$input = $this->ORTissueTransaction->make_all_mandatory_fields();

			$response = $this->postToApi($input, 'Case3');

			$response->assertStatus(200);
		}

		/**
		 * Case 4:
		 *
		 * Create 1 transaction with no value in Refund Rules field.
		 *
		 * Result:
		 * "This transaction will not be imported to CCH.
		 * ""responseCode"":""0010"",
		 * ""responseDescription"":""refundrule is missing"""
		 * @test
		 */
		public function failed_transaction_without_refund_rule(): void
		{
			$input = $this->ORTissueTransaction->make_transaction_without_refund_rule();

			$response = $this->postToApi($input, 'Case4');

			$response->assertStatus(200);
		}

		/**
		 * Case 5:
		 * "1. In ""Issue Transaction"" , fill in Tourist and Transactions info, with Transaction has amount less than the minimum eligible amount.
		 *
		 * Example:
		 * Receipt #1:
		 * Purchase Item Gross Amount: 89.00
		 *
		 * 2. Click Submit Button
		 * "
		 * Result:
		 * "CCH system saves the transaction into the database, with Issued Status = ""Issued"".
		 *
		 * ""responseCode"": 0000,
		 * ""responseMessage"": ""Issue transaction successful""
		 *
		 * Note: The purchase amount is less than SGD 100, which is the minimum eligible amount, but the transaction is still successfully saved into CCH system, as specified in 4.2.2.4 CRA/IR-CCH Interface: Issue / Update Transaction [URS_CP]."
		 * @test
		 */
		public function success_transaction_with_amount_less_than_minimum_eligible_amount(): void
		{
			$input = $this->ORTissueTransaction->make_transaction_with_amount_less_than_minimum_eligible_amount();

			$response = $this->postToApi($input, 'Case5');

			$response->assertStatus(200);
		}

		/**
		 * Additional scenarios from NCS
		 * @test
		 */
		public function additional_scenario(): void
		{
			$input = $this->ORTissueTransaction->make_additional_scenario();

			$response = $this->postToApi($input, 'ETC');

			$response->assertStatus(200);
		}
		
		/**
		 * @param array $input
		 * @param string $fileName
		 * @return TestResponse
		 */
		private function postToApi(array $input, string $fileName): TestResponse
		{
			$response = $this->json('POST', '/api/etrs', $input);
			var_dump(json_encode($input));
			var_dump($response->getContent());
			$this->saveToFile($input, $response->getContent(), $fileName);

			return $response;
		}
	}
<?php

	namespace Tests\Unit\ORT;

	use Illuminate\Foundation\Testing\TestResponse;
	use Tests\ApiTest;

	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 25/2/2019
	 * Time: 4:01 PM
	 */
	class GetStatusByPeriodApiTest extends ApiTest
	{

		/**
		 * @param $input
		 * @param $fileName
		 * @return TestResponse
		 */
		private function postToStatusByPeriodApi($input, $fileName): TestResponse
		{
			$response = $this->json('POST', '/api/statusByPeriod', $input);

			var_dump(json_encode($input));
			var_dump($response->getContent());

			$this->saveToFile($input, $response->getContent(), $fileName);

			return $response;
		}

		/**
		 * Case 12:
		 * "1. In ""Get Transaction Status (Batch) By Period"", enter the following values:
		 * Datetime: <invalid datetime,
		 * e.g. 2018-04-44T15.23.25>
		 *
		 * 2. Click Retrieve Button"
		 * Result:
		 * "CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to invalid datetime parameter
		 *
		 * ""responseCode"": 1004,
		 * ""responseMessage"": ""Invalid input parameter""
		 * "
		 * @test
		 *
		 */
		public function failed_get_transaction_with_invalid_datetime(): void
		{
			$input = $this->ORTstatusByPeriod->make_transaction_with_invalid_datetime();

			$response = $this->postToStatusByPeriodApi($input, 'Case12');

			$response->assertStatus(200);
		}

		/**
		 * Case 13:
		 * "1. In ""Get Transaction Status (Batch) By Period"", enter the following values:
		 * Datetime: <valid datetime>
		 * which has transactions in UAT DB
		 *
		 * 2. Click Retrieve Button"
		 * Result:
		 * "CCH system returns the result, with all the transactions belong to the CRA/IR.
		 *
		 * Each transaction in the results consist of either below fields:
		 * 1. Issued Status Update Event:
		 * CRA/IR ID, Doc ID, Issued Status, Transaction Update Date/Time
		 * 2. Customs Status Update Event:
		 * CRA/IR ID, Doc ID, Custom Status, Custom Stamp Date/Time, Transaction Update Date/Time, Exported Transaction Gross Amount, Refund Amount
		 * 3. Refund Status Update Event:
		 * CRA/IR ID, Doc ID, Refund Status, Refund Date/Time, Refund Method, Refund Amount, CRC or DTRO ID, Transaction Update Date/Time
		 * "
		 * @test
		 */
		public function success_get_all_transaction_by_period(): void
		{
			$input = $this->ORTstatusByPeriod->make_transaction_with_valid_datetime();

			$response = $this->postToStatusByPeriodApi($input, 'Case13');

			$response->assertStatus(200);
		}

	}
<?php

	namespace Tests\Unit\UAT;

	use Tests\ApiTest;

	class GetStatusByDocIdApiTest extends ApiTest
	{

		private function postToStatusByDocId($input, $fileName)
		{
			$response = $this->json('POST', '/api/statusById', $input);

			$this->saveToFile($input, $response->getContent(), $fileName);

			return $response;
		}

		/**
		 * Case 23
		 *
		 * Scenario:
		 * CRA/IR to send request for status from CCH using Doc ID
		 *
		 * Expected Result:
		 * Retrieve the latest updates of all CRA/IR-issued Transactions (e.g. Issued / Customs / Refund Status).
		 *
		 * Key fields for Issued Status Update Event:
		 * • CRA/IR ID
		 * • Doc-ID
		 * • Issued Status
		 * • Transaction Update Date / Time
		 *
		 * Key fields for Customs Status Update Event:
		 * • CRA/IR ID
		 * • Doc-ID
		 * • Customs Status
		 * • Customs Stamp Date / Time
		 * • Transaction Update Date / Time
		 * • Exported Transaction Gross Amount
		 * • Refund Amount
		 *
		 * Key fields for Refund Status Update Event:
		 * • CRA/IR ID
		 * • Doc-ID
		 * • Refund Status
		 * • Refund Date / Time
		 * • Refund Method
		 * • Refund Amount
		 * • CRC or DTRO ID
		 * • Transaction Update Date / Time
		 *
		 * @test
		 */

		public function success_get_transaction_with_valid_doc_id(): void
		{
			$input = $this->statusByDocId->make_transaction_with_valid_doc_id();

			$response = $this->postToStatusByDocId($input, 'Case23');

			$response->assertStatus(200);

		}

		/**
		 * Case 24
		 *
		 * Scenario
		 * 1. In "Get Transaction Status (Batch) By Doc Id", following values:
		 * Doc Id: <invalid Doc Id, e.g. "zzzzz">
		 *
		 * 2. Click Retrieve Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to invalid Doc ID.
		 *
		 * "responseCode": 1004,
		 * "responseMessage": "invalid_input_parameter"
		 *
		 * @test
		 */

		public function fail_get_transaction_with_invalid_doc_id(): void
		{
			$input = $this->statusByDocId->make_transaction_with_invalid_doc_id();

			$response = $this->postToStatusByDocId($input, 'Case24');

			$response->assertStatus(200);
		}

		/**
		 * Case25
		 *
		 * Scenario
		 * 1. In "Get Transaction Status (Batch) By Doc Id", following values:
		 * Doc ID: <valid Doc ID that exists in UAT DB>
		 *
		 * 2. Click Retrieve Button
		 *
		 * Expected Result:
		 * CCH system returns the transaction matching the Doc ID.
		 *
		 * Each transaction in the results consist of either below fields:
		 * 1. Issued Status Update Event:
		 * CRA/IR ID, Doc ID, Issued Status, Transaction Update Date/Time
		 * 2. Customs Status Update Event:
		 * CRA/IR ID, Doc ID, Custom Status, Custom Stamp Date/Time, Transaction Update Date/Time, Exported Transaction Gross Amount, Refund Amount
		 * 3. Refund Status Update Event:
		 * CRA/IR ID, Doc ID, Refund Status, Refund Date/Time, Refund Method, Refund Amount, CRC or DTRO ID, Transaction Update Date/Time
		 *
		 * @test
		 */

		public function success_get_transaction_with_exist_doc_id(): void
		{
			$input = $this->statusByDocId->make_transaction_with_exist_doc_id();

			$response = $this->postToStatusByDocId($input, 'Case25');

			$response->assertStatus(200);
		}

		/**
		 * Case 56
		 *
		 * Scenario
		 * 1. In "Get Transaction Status (Batch) By Doc Id", following values:
		 * Doc Id: <blank>
		 *
		 * 2. Click Retrieve Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to blank Doc ID.
		 *
		 * "responseCode": 1004,
		 * "responseMessage": "invalid_input_parameter"
		 *
		 * @test
		 */

		public function fail_get_transaction_with_blank_doc_id(): void
		{
			$input = $this->statusByDocId->make2_transaction_with_blank_doc_id();

			$response = $this->postToStatusByDocId($input, 'Case56');

			$response->assertStatus(200);
		}

		/**
		 * Case 58
		 *
		 * Scenario:
		 * 1. In "Get Transaction Status (Batch) By Doc Id", following values:
		 * Doc ID: <valid Doc ID that does not exist in UAT DB>
		 *
		 * 2. Click Retrieve Button
		 *
		 * Expected Result:
		 * CCH system returns the result, with 0 transactions.
		 *
		 * "responseCode": 1001
		 * "responseMessage": "Transactions not found in db."
		 *
		 * @test
		 */

		public function fail_get_transaction_with_non_exist_doc_id(): void
		{
			$input = $this->statusByDocId->make_transaction_with_non_exist_doc_id();

			$response = $this->postToStatusByDocId($input, 'Case58');

			$response->assertStatus(200);
		}


	}

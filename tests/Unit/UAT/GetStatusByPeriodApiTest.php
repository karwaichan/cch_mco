<?php

	namespace Tests\Unit\UAT;

	use Tests\ApiTest;

	class GetStatusByPeriodApiTest extends ApiTest
	{

		private function postToStatusByPeriodApi($input, $fileName)
		{
			$response = $this->json('POST', '/api/statusByPeriod', $input);

			$this->saveToFile($input, $response->getContent(), $fileName);

			return $response;
		}

		/**
		 * case 20
		 *
		 *  Scenario:
		 * 1. In "Get Transaction Status (Batch) By Period", enter the following values:
		 * Datetime: <blank>
		 *
		 * 2. Click Retrieve Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to blank datetime parameter
		 *
		 * "responseCode": 1004,
		 * "responseMessage": "invalid_input_parameter"
		 *
		 * @test
		 */

		public function fail_get_transaction_status_without_date_time(): void
		{
			$input = $this->UATstatusByPeriod->make_transaction_status_without_date_time();

			$response = $this->postToStatusByPeriodApi($input, 'Case20');

			$response->assertStatus(200);
		}

		/**
		 * Case 21
		 *
		 * Scenario:
		 * 1. In "Get Transaction Status (Batch) By Period", enter the following values:
		 * Datetime: <future datetime, e.g.
		 * 2019-04-21T15.23.25>
		 *
		 * 2. Click Retrieve Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to future datetime parameter
		 *
		 * "responseCode": 1003,
		 * "responseMessage": "DatetimeUtc cannot be a future date."
		 *
		 * @test
		 */

		public function fail_get_transaction_with_future_date_time(): void
		{
			$input = $this->UATstatusByPeriod->make_transaction_with_future_date_time();

			$response = $this->postToStatusByPeriodApi($input, 'Case21');

			$response->assertStatus(200);
		}

		/**
		 * Case 22
		 *
		 * Scenario:
		 * 1. In "Get Transaction Status (Batch) By Period", enter the following values:
		 * Datetime: <valid datetime>
		 * which has transactions in UAT DB
		 *
		 * 2. Click Retrieve Button
		 *
		 * Expected Result:
		 * CCH system returns the result, with all the transactions belong to the CRA/IR.
		 *
		 * Each transaction in the results consist of either below fields:
		 * 1. Issued Status Update Event:
		 * CRA/IR ID, Doc ID, Issued Status, Transaction Update Date/Time
		 * 2. Customs Status Update Event:
		 * CRA/IR ID, Doc ID, Custom Status, Custom Stamp Date/Time, Transaction Update Date/Time, Exported Transaction Gross Amount, Refund Amount
		 * 3. Refund Status Update Event:
		 * CRA/IR ID, Doc ID, Refund Status, Refund Date/Time, Refund Method, Refund Amount, CRC or DTRO ID, Transaction Update Date/Time
		 *
		 * @test
		 */

		public function success_get_transaction_with_valid_date_time(): void
		{
			$input = $this->UATstatusByPeriod->make_transaction_with_valid_date_time();

			$response = $this->postToStatusByPeriodApi($input, 'Case22');

			$response->assertStatus(200);
		}


		/**
		 * Case 47
		 *
		 * Scenario:
		 * CRA/IR to send request for getstatus from CCH.
		 *
		 * Expected Result:
		 * Retrieve the latest updates of all CRA/IR-issued Transactions (e.g. Issued / Customs / Refund Status).
		 *
		 * Key fields for Issued Status Update Event:
		 * • CRA/IR ID
		 * • Doc-ID
		 * • Issued Status
		 * • Transaction Update Date / Time
		 *
		 * Key fields for Customs Status Update Event:
		 * • CRA/IR ID
		 * • Doc-ID
		 * • Customs Status
		 * • Customs Stamp Date / Time
		 * • Transaction Update Date / Time
		 * • Exported Transaction Gross Amount
		 * • Refund Amount
		 *
		 * Key fields for Refund Status Update Event:
		 * • CRA/IR ID
		 * • Doc-ID
		 * • Refund Status
		 * • Refund Date / Time
		 * • Refund Method
		 * • Refund Amount
		 * • CRC or DTRO ID
		 * • Transaction Update Date / Time
		 *
		 * @test
		 */

		public function success_get_transaction_status(): void
		{
			$input = $this->UATstatusByPeriod->make_get_transaction_status();

			$response = $this->postToStatusByPeriodApi($input, 'Case47');

			$response->assertStatus(200);
		}

		/**
		 * Case 48
		 *
		 * Scenario:
		 * 1. In "Get Transaction Status (Batch)"
		 *
		 * 2. Click Retrieve Button
		 *
		 * Pre-Condition:
		 * There are no transactions in UAT DB that belong to the specified CRA/IR ID
		 *
		 * Expected Result:
		 * CCH system returns the result, with 0 transactions.
		 *
		 * "responseCode": 1001
		 * "responseMessage": "Transactions not found in db."
		 *
		 * @test
		 */

		public function fail_get_transcation_status_on_no_transaction(): void
		{
			$input = $this->UATstatusByPeriod->make_transcation_status_on_no_transaction();

			$response = $this->postToStatusByPeriodApi($input, 'Case48');

			$response->assertStatus(200);
		}


		/**
		 * Case 50
		 *
		 * Scenario:
		 * 1. In "Get Transaction Status (Batch) By Period", enter the following values:
		 * Datetime: <invalid datetime,
		 * e.g. 2018-04-44T15.23.25>
		 *
		 * 2. Click Retrieve Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to invalid datetime parameter
		 *
		 * "responseCode": 1004,
		 * "responseMessage": "invalid_input_parameter"
		 *
		 * @test
		 */

		public function fail_get_transaction_with_invalid_date_time(): void
		{
			$input = $this->UATstatusByPeriod->make_transaction_with_invalid_date_time();

			$response = $this->postToStatusByPeriodApi($input, 'Case50');

			$response->assertStatus(200);
		}

		/**
		 * Case 52
		 *
		 * Scenario
		 * 1. In "Get Transaction Status (Batch) By Period", enter the following values:
		 * Datetime: <more than 3 months earlier than today,
		 * e.g. 2018-05-12T15.23.25>
		 *
		 * 2. Click Retrieve Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to datetime parameter is more than 3 months earlier from today
		 *
		 * "responseCode": 1002,
		 * "responseMessage": "DatetimeUtc is more than 3 months earlier from today."
		 *
		 * @test
		 */

		public function fail_get_transaction_with_3_months_earlier_date_time(): void
		{
			$input = $this->UATstatusByPeriod->make_transaction_with_3_months_earlier_date_time();

			$response = $this->postToStatusByPeriodApi($input, 'Case52');

			$response->assertStatus(200);
		}

		/**
		 * Case 53
		 *
		 * Scenario:
		 * 1. In "Get Transaction Status (Batch) By Period", enter the following values:
		 * Datetime: <valid datetime>
		 * which does not have any transactions in UAT DB
		 *
		 * 2. Click Retrieve Button
		 *
		 * Expected Result:
		 * CCH system returns the result, with 0 transactions.
		 *
		 * "responseCode": 1001
		 * "responseMessage": "Transactions not found in db."
		 * @test
		 */

		public function fail_get_transaction_for_date_time_with_no_transaction(): void
		{
			$input = $this->UATstatusByPeriod->make_transaction_for_date_time_with_no_transaction();

			$response = $this->postToStatusByPeriodApi($input, 'Case53');

			$response->assertStatus(200);
		}



	}

<?php

	namespace Tests\Unit\UAT;

	use App\Http\ApiRequest;
	use App\ValueObjects\AccessToken;
	use App\ValueObjects\JWT;
	use Carbon\Carbon;
	use Tests\ApiTest;

	/**
	 * URL : https://tourego.sharepoint.com/:x:/r/_layouts/15/Doc.aspx?sourcedoc=%7B8e344439-7be1-44aa-93ad-743d29c8329b%7D&action=edit&activeCell=%27Test%20Cases%27!N1&wdInitialSession=7a4a029a-71c5-4449-9511-c3bdec621e73&wdRldC=1
	 *
	 * Test for issue transaction function
	 *
	 * Class EtrsIssueTransactionApiTest
	 * @package Tests\Unit
	 */
	class EtrsIssueTransactionApiTest extends ApiTest
	{
		/**
		 * Test success standalone access token call
		 * @test
		 */
		public function access_token_success(): void
		{
			//check for valid access token
			$accessTokenJson  = (new ApiRequest())->postEtrsRequest(new AccessToken(new JWT()));
			$accessTokenArray = json_decode($accessTokenJson, true);
			//assert result
			$this->assertArrayHasKey('access_token', $accessTokenArray);
		}
		//========================================================================================================
		/**
		 * Section 1
		 *
		 * Test Case Name : Receipt Consolidation and Combination Rules
		 */


		/**
		 * Case 1
		 *
		 * Scenario:
		 * "Create a transaction with 4 receipts tagged:
		 *
		 * 1) Receipt date: 17/12/2018, amount = $20
		 * 2) Receipt date: 17/12/2018, amount = $25
		 * 3) Receipt date: 17/12/2018, amount = $40
		 * 4) Receipt date: 17/12/2018, amount = $15
		 *
		 * Validate the transaction."
		 *
		 * Expected Result:
		 * Receipts 1-4 cannot be consolidated to issue a transaction as it takes 4 receipts to form a combined total of >=$100.
		 *
		 * Therefore, this eTRS transaction is invalid and shouldn’t be approved even if it is present in the system.
		 * @test
		 */
		public function fail_consolidation_3_receipts_less_than_100(): void
		{
			$input = $this->UATissueTransaction->make_receipts_less_than_100();

			$response = $this->postToApi($input, 'Case1');

			$response->assertStatus(200);
		}

		/**
		 * Case 2
		 *
		 * Scenario:
		 * "Create a transaction with 3 receipts tagged:
		 *
		 * 1) Receipt date: 17/12/2018, amount = $15
		 * 2) Receipt date: 17/12/2018, amount = $10
		 * 3) Receipt date: 17/12/2018, amount = $80
		 *
		 * Validate the transaction.
		 *
		 * Expected Result :
		 *
		 * Receipts 1-3 can be consolidated to issue a transaction as it takes 3 receipts to form a combined total of >=$100.
		 *
		 * Therefore, this transaction is valid and approvable.
		 * @test
		 */
		public function success_consolidation_3_receipts_more_than_100(): void
		{
			$input = $this->UATissueTransaction->make_3_receipts_more_than_100();

			$response = $this->postToApi($input, 'Case2');

			$response->assertStatus(200);
		}

		/**
		 * Case 3
		 *
		 * Scenario:
		 * "Create a transaction with 4 receipts tagged:
		 *
		 * 1) Receipt date: 17/12/2018, amount = $15
		 * 2) Receipt date: 17/12/2018, amount = $10
		 * 3) Receipt date: 17/12/2018, amount = $80
		 * 4) Receipt date: 18/12/2018, amount = $120
		 *
		 * Validate the transaction."
		 *
		 * Expected Result :
		 * Receipts 1-3 can be consolidated to issue a transaction as it takes 3 receipts to form a combined total of >=$100.
		 *
		 * Receipt 4 cannot be consolidated with the other 3 receipts to issue a transaction as itself is <$100 and is from a different receipt date.
		 *
		 * Therefore, this transaction is only valid based on the first 3 receipts.
		 * @test
		 */
		public function success_consolidate_3_receipts_more_than_100_same_day_and_1_receipt_different_day(): void
		{
			$input = $this->UATissueTransaction->make_3_receipts_more_than_100_same_day_and_1_receipt_different_day();

			$response = $this->postToApi($input, 'Case3');

			$response->assertStatus(200);
		}

		/**
		 * Case 4:
		 *
		 * Scenario:
		 * "Create a transaction with 6 receipts tagged:
		 *
		 * 1) Receipt date: 17/12/2018, amount = $15
		 * 2) Receipt date: 17/12/2018, amount = $10
		 * 3) Receipt date: 17/12/2018, amount = $80
		 * 4) Receipt date: 17/12/2018, amount = $10
		 * 5) Receipt date: 18/12/2018, amount = $70
		 * 6) Receipt date: 18/12/2018, amount = $40
		 *
		 * Validate the transaction."
		 * Expected Result:
		 *
		 * "Receipts 1-4 can be consolidated to issue a transaction as it takes 3 receipts (of the highest values) to form a combined total of >=$100.
		 *
		 * Receipts 5-6 can be consolidated to issue a transaction as it takes 2 receipts to form a combined total of >=$100.
		 *
		 * Receipts 5-6 can also be combined with receipts 1-4 to issue a transaction.
		 *
		 * Therefore, this transaction is valid and approvable."
		 * @test
		 */
		public function success_consolidate_4_receipts_more_than_100_same_day_and_2_receipt_more_than_100_different_day(): void
		{
			$input = $this->UATissueTransaction->make_consolidate_4_receipts_more_than_100_same_day_and_2_receipt_more_than_100_different_day();

			$response = $this->postToApi($input, 'Case4');

			$response->assertStatus(200);
		}

		/**
		 * Case 5:
		 *
		 * Scenario:
		 * "Create a transaction with 4 receipts tagged:
		 *
		 * 1) Receipt date: 17/12/2018, amount = $150
		 * 2) Receipt date: 17/12/2018, amount = $110
		 * 3) Receipt date: 17/12/2018, amount = $200
		 * 4) Receipt date: 17/12/2018, amount = $80
		 *
		 * Validate the transaction."
		 * Expected Result:
		 * Receipts 1-3 can each be issued as a transaction.
		 *
		 * Receipt 4 cannot be issued as a transaction by itself but it can be combined in any combinations with receipts 1-3. All the receipts have the same date.
		 *
		 * Therefore, this transaction is valid and approvable.
		 *
		 * @test
		 */

		public function success_issued_with_3_receipts_more_than_100_and_1_receipt_less_than_100(): void
		{
			$input = $this->UATissueTransaction->make_3_receipts_more_than_100_and_1_receipt_less_than_100();

			$response = $this->postToApi($input, 'Case5');

			$response->assertStatus(200);
		}

		/**
		 * Case 6:
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Tourist Passport Number field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_tourist_passport_number(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_tourist_passport_number();

			$response = $this->postToApi($input, 'Case6');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 7:
		 *
		 * Scenario:
		 * 1. In "Issue Transaction", key in some tourist/transactions info, but with invalid Doc ID,
		 * e.g. Doc ID = "aaa"
		 * 2. Click Submit Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to invalid Doc ID.
		 *
		 * The Response Code/Desc is:
		 * "responseCode": 0020,
		 * "responseMessage": "DocID's length is not supported"
		 *
		 * @test
		 */

		public function fail_create_transaction_with_invalid_document_id(): void
		{
			$input = $this->UATissueTransaction->make_transaction_with_invalid_document_id();

			$response = $this->postToApi($input, 'Case7');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 8
		 *
		 * Scenario:
		 * 1. In "Issue Transaction" , key in all necessary info, but with Doc ID that already exists in CCH system
		 * (Please use the Doc ID generated for the test case S. No :2)
		 * 2. Click Submit Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 *
		 * ResponseCode: 0120
		 * ResponseMessage: "This docId was already submitted with another transaction"
		 *
		 * @test
		 */

		public function fail_create_transaction_with_existing_doc_id(): void
		{
			$input = $this->UATissueTransaction->make_transaction_with_existing_doc_id();

			$response = $this->postToApi($input, 'Case8');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 9
		 *
		 * Scenario
		 * "1. In ""Issue Transaction"" , fill in all Mandatory Fields, and all Fields in correct format.
		 *
		 * Example:
		 *
		 * Doc ID: 20011301201710005002
		 * Refund Rule: 13
		 * Shop Id: 1001
		 * Shop Name 1: Best Denki Compass One
		 * Shop Name 2:
		 * Shop Address 1: Compass One #01-24, Sengkang
		 * Shop Address 2:
		 * Shop Postal Code: 543102
		 * Retailer GST Reg. Number: BEST234567
		 * Issuing Solution Activation Date: 2018-03-01
		 * Tourist DOB: 1980-11-26
		 * Tourist Passport Number & Nationality:
		 * Issue Date Time: 2017-11-20T05:59:59
		 * Provisional Refund Amount: 25.20
		 * Service Fee Amount: 3.00
		 * Test Transaction indicator: No
		 * Receipt Number: B11023456
		 * Receipt Date/Time: 2017-11-20T15:22:21
		 * GST Rate: 7.00
		 * Purchase Item Description:
		 * Purchase Item Detail Description:
		 * Purchase Item Gross Amount: 589.65
		 * Purchase Item Quantity: 1
		 * Purchase Item Standard Goods Category: 121
		 * Receipt Number: B11030294
		 * Receipt Date/Time: 2017-11-20T15:22:25
		 * GST Rate: 7.00
		 * Purchase Item Description:
		 * Purchase Item Detail Description:
		 * Purchase Item Gross Amount: 203.62
		 * Purchase Item Quantity: 2
		 * Purchase Item Standard Goods Category: 118
		 *
		 * 2. Click Submit Button"
		 *
		 * Expected Result:
		 * CCH system saves the transaction into the database, with Issued Status = "Issued".
		 *
		 * "responseCode": 0000,
		 * "responseMessage": "Issue <transaction doc id> successful"
		 *
		 * @test
		 */

		public function success_transaction_with_all_mandatory_fields(): void
		{
			$input = $this->UATissueTransaction->make_transaction_with_all_mandatory_fields();

			$response = $this->postToApi($input, 'Case9');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 10
		 *
		 * Scenario :
		 * 1. In "Issue Transaction" , fill in Tourist and Transactions info, with Transaction has 2 receipts which are of different receipt dates.
		 *
		 * Example:
		 * Receipt #1
		 * Receipt Date: 2018-08-18
		 * Receipt #2
		 * Receipt Date: 2018-08-19
		 *
		 * 2. Click Submit Button
		 *
		 * Expected Result:
		 * CCH system saves the transaction into the database, with Issued Status = "Issued".
		 *
		 * "responseCode": 0000,
		 * "responseMessage": "Issue transaction successful"
		 *
		 * Note: The transaction has 2 receipts which are of different receipt dates, but the transaction is still successfully
		 * saved into CCH system, as specified in 4.2.2.4 CRA/IR-CCH Interface: Issue / Update Transaction [URS_CP].
		 *
		 * @test
		 */

		public function success_create_transaction_with_2_receipts_with_different_dates(): void
		{
			$input = $this->UATissueTransaction->make_transaction_with_2_receipts_with_different_dates();

			$response = $this->postToApi($input, 'Case10');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 7:
		 *
		 * Scenario :
		 *Create 1 transaction with all the mandatory fields included:
		 *
		 * Mandatory fields include (20 fields):
		 * 1) Doc ID
		 * 2) Refund Rules
		 * 3) Shop ID
		 * 4) Shop Name 1
		 * 5) Shop Postal Code
		 * 6) Retailer GST Registration Number
		 * 7) Issuing Solution Activation Date
		 * 8) Tourist Passport Number
		 * 9) Tourist Nationality
		 * 10) Issued Date Time
		 * 11) Provisional Refund Amount
		 * 12) Service Fee Amount
		 * 13) Receipt Number
		 * 14) Receipt Date/Time
		 * 15) GST Rate
		 * 16) Purchase Item Gross Amount
		 * 17) Purchase Item Standard Goods Category
		 *
		 * Expected Result:
		 * The transaction with all the mandatory fields entered will be imported successfully.
		 *
		 * @test
		 */

		public function success_with_all_mandatory_fields(): void
		{
			$input = $this->UATissueTransaction->make_success_with_all_mandatory_field();

			$response = $this->postToApi($input, 'Case7');

			$response->assertStatus(200);

		}

		/**
		 * Case 8
		 * Scenario:
		 * Create 1 transaction with no value in Doc ID field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 *
		 * @test
		 */

		public function fail_create_transaction_without_doc_id(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_doc_id();

			$response = $this->postToApi($input, 'Case8');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);

		}

		/**
		 * Case 9:
		 * Scenario:
		 * Create 1 transaction with no value in Refund Rules field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_refund_rule(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_refund_rule();

			$response = $this->postToApi($input, 'Case9');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 10
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Shop ID field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_shop_id(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_shop_id();

			$response = $this->postToApi($input, 'Case10');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 11
		 *
		 * Scenario :
		 * Create 1 transaction with no value in Shop Name 1 field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_shop_name(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_shop_name();

			$response = $this->postToApi($input, 'Case11');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);

		}

		/**
		 * Case 12:
		 *
		 * Scenario:
		 *Create 1 transaction with no value in Shop Postal Code field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_shop_postcode(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_shop_postcode();

			$response = $this->postToApi($input, 'Case12');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 13:
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Retailer GST Registration Number field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_gst_number(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_gst_number();

			$response = $this->postToApi($input, 'Case13');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 14:
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Issuing Solution Activation Date field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_shop_activation_date(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_shop_activation_date();

			$response = $this->postToApi($input, 'Case14');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 16:
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Tourist Nationality field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_tourist_nationality(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_tourist_nationality();

			$response = $this->postToApi($input, 'Case16');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 17:
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Issued Date Time field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH
		 * @test
		 */

		public function fail_create_transaction_without_issue_date(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_issue_date();

			$response = $this->postToApi($input, 'Case17');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 18:
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Provisional Refund Amount field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_provisional_refund_amount(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_provisional_refund_amount();

			$response = $this->postToApi($input, 'Case18');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 19:
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Service Fee Amount field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */
		public function fail_create_transaction_without_service_fee_amount(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_service_fee_amount();

			$response = $this->postToApi($input, 'Case19');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 20:
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Receipt Number field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_receipt_number(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_receipt_number();

			$response = $this->postToApi($input, 'Case20');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case21:
		 *
		 *    Scenario:
		 *    Create 1 transaction with no value in Receipt Date/Time field.
		 *
		 *    Expected Result:
		 *    This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_receipt_date_time(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_receipt_date_time();

			$response = $this->postToApi($input, 'Case21');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 22
		 *
		 * Scenario:
		 * Create 1 transaction with no value in GST Rate field.
		 *
		 * Expected Result:
		 *
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_gst_rate(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_gst_rate();

			$response = $this->postToApi($input, 'Case22');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 23:
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Purchase Item Gross Amount field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_item_gross_amount(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_item_gross_amount();

			$response = $this->postToApi($input, 'Case23');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 24:
		 *
		 * Scenario:
		 * Create 1 transaction with no value in Purchase Item Standard Goods Category field.
		 *
		 * Expected Result:
		 * This transaction will not be imported to CCH.
		 * @test
		 */

		public function fail_create_transaction_without_item_category(): void
		{
			$input = $this->UATissueTransaction->make_transaction_without_item_category();

			$response = $this->postToApi($input, 'Case24');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 26:
		 *
		 * Scenario:
		 * 1. In "Issue Transaction", key in some tourist/transactions info, but with invalid Retailer GST Reg. Number,
		 * e.g. Retailer GST Reg. Number = "abcd"
		 * 2. Click Submit Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to invalid Retailer GST Registration Number.
		 *
		 * The Response Code/Desc is:
		 * "responseCode": 0020,
		 * "responseMessage": "Retailer GST Registration Number's length is not supported"
		 * @test
		 */

		public function fail_create_transaction_with_invalid_retailer_gst_register_number(): void
		{
			$input = $this->UATissueTransaction->make_transaction_with_invalid_retailer_gst_register_number();

			$response = $this->postToApi($input, 'Case26');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 27:
		 *
		 * Scenario:
		 * 1. In "Issue Transaction" , key in some tourist/transactions info, but with invalid Tourist Date of Birth,
		 * e.g. Tourist Date of Birth = "2018-14-04"
		 * 2. Click Submit Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to invalid Tourist Date of Birth.
		 *
		 * ResponseCode: 0030
		 * ResponseMessage: Tourist DOB's type is incorrect
		 *
		 * @test
		 */

		public function fail_create_transaction_with_invalid_tourist_date_of_birth(): void
		{
			$input = $this->UATissueTransaction->make_transaction_with_invalid_tourist_date_of_birth();

			$response = $this->postToApi($input, 'Case27');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 28
		 *
		 * Scenario:
		 *
		 *1. In "Issue Transaction" , key in some tourist/transactions info, but with invalid Purchase Item Quantity,
		 * e.g. Purchase Item Quantity = "abcd"
		 * 2. Click Submit Button
		 *
		 * Expected Result:
		 * CCH system returns appropriate error code/message back to CRA/IR system:
		 * Error message due to invalid Quantity.
		 * "responseCode": 0030,
		 * "responseMessage": "Purchase Item Quantity's type is incorrect"
		 *
		 * @test
		 *
		 *
		 */

		public function fail_create_transaction_with_invalid_purchase_item_quantity(): void
		{
			$input = $this->UATissueTransaction->make_transaction_with_invalid_purchase_item_quantity();

			$response = $this->postToApi($input, 'Case28');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * Case 31
		 *
		 * Scenario
		 * 1. In "Issue Transaction" , fill in Tourist and Transactions info, with Transaction has amount less than the minimum eligible amount.
		 *
		 * Example:
		 * Receipt #1:
		 * Purchase Item Gross Amount: 89.00
		 *
		 * 2. Click Submit Button
		 *
		 * Expected Result:
		 * CCH system saves the transaction into the database, with Issued Status = "Issued".
		 *
		 * "responseCode": 0000,
		 * "responseMessage": "Issue transaction successful"
		 *
		 * Note: The purchase amount is less than SGD 100, which is the minimum eligible amount, but the transaction is still
		 * successfully saved into CCH system, as specified in 4.2.2.4 CRA/IR-CCH Interface: Issue / Update Transaction [URS_CP].
		 * @test
		 */

		public function success_transaction_with_amount_less_than_minimum_eligible(): void
		{
			$input = $this->UATissueTransaction->make_transaction_with_amount_less_than_minimum_eligible();

			$response = $this->postToApi($input, 'Case31');

			$error = json_decode($response->getContent(), true);

			$this->assertArrayHasKey('error', $error);
		}

		/**
		 * @param $input
		 * @param $fileName
		 */
		private function postToApi($input, $fileName)
		{
			$response = $this->json('POST', '/api/etrs', $input);
			var_dump(json_encode($input));
			var_dump($response->getContent());
			$this->saveToFile($input, $response->getContent(), $fileName);

			return $response;
		}

	}
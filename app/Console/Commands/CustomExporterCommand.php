<?php

	namespace App\Console\Commands;

	use Carbon\Carbon;
	use GuzzleHttp\Client;
	use Illuminate\Console\Command;
	use Illuminate\Support\Facades\Storage;

	class CustomExporterCommand extends Command
	{
		/**
		 * The name and signature of the console command.
		 *
		 * @var string
		 */
		protected $signature = 'exporter:export';

		/**
		 * The console command description.
		 *
		 * @var string
		 */
		protected $description = 'Get all Custom \'s Exporter files from CCH2';

		/**
		 * Create a new command instance.
		 *
		 * @return void
		 */
		public function __construct()
		{
			parent::__construct();
		}

		/**
		 * Execute the console command.
		 *
		 * @return mixed
		 */
		public function handle()
		{
			$this->info('Process starting....');
			$this->customExporter();
			$this->info('Process completed...');
		}

		/**
		 * CCH2 custom exporter
		 */
		private function customExporter(): void
		{
			//set status code to null to accommodate CCH response code.
			$customExporterStatusCode = null;
			//double array to associate high resources of array merge in while loop
			$allCustomExporterFile = [[]];

			/**
			 *1. get All from previous day (100 transactions)
			 *2. assigned it to an array if the date is yesterday.
			 *
			 * 3. if after 100 transactions there last element date is still yesterday, set the dateTime to that date and query again is from the previous day from yesterday.
			 **/

			$dateTime = $initialDateTime = Carbon::now()->setTimezone('GMT+8')->subDay()->endOfDay();

			$this->info('Initializing Request to CCH2 ...');

			while($dateTime->isSameDay($initialDateTime)){
				$response = (new Client())->request('POST', 'https://access.tourego.com/api/statusByPeriod', ['form_params' => ['DateTime' => $dateTime->toDateTimeString()]]);

				$customExporterArray = json_decode($response->getBody()->getContents(), true);

				$customExporterStatusCode = $customExporterArray['responseCode'] ?? null;

				if ((int)$customExporterStatusCode !== 1001) {
					$allCustomExporterFile[] = $customExporterArray;
					$data = $this->formatResult($customExporterArray);
					$firstArrayForDateChecking = current($data);
					$dateTime = Carbon::parse($firstArrayForDateChecking['transactionUpdatedDateTime']);
				}
			}

			$this->info('Merging all responses from CCH ...');

			$allCustomExporterFile = array_merge(...$allCustomExporterFile);
			array_shift($allCustomExporterFile);

			$this->info('Searching for yesterday transactions ...');
			$onlyYesterdayTransactions = array_filter($allCustomExporterFile, function ($value, $key) use($initialDateTime) {
				if (Carbon::parse($value['transactionUpdatedDateTime'])->isSameDay($initialDateTime)) {
					return $value;
				}
			}, ARRAY_FILTER_USE_BOTH);

			if (!empty($onlyYesterdayTransactions)) {
				$this->info('Found ' . \count($onlyYesterdayTransactions) . ' elements ...');
				$this->info('Saving files to storage ...');
				//merge all to a single json
				$s3 = Storage::disk('s3');

				$s3File = '/customs_exporter/' . Carbon::now()->setTimezone('GMT+8')->format('Ymd') . '/CustomsStampedTransaction-' . Carbon::now()->setTimezone('GMT+8')->format('Ymd') . '.json';
				//write to S3
				$s3->put($s3File, json_encode(array_values($onlyYesterdayTransactions)));
			}

			if (empty($onlyYesterdayTransactions)) {
				$this->info('Nothing was found. =( ...');
				$this->error('There are no transactions for yesterday, DATE: ' . Carbon::now()->subDay()->format('Y-m-d'));
			}
		}

		private function formatResult(array $customExporter)
		{
			return array_filter($customExporter, function ($value, $key) {
				if ($key !== 'error') {
					return $value;
				}
			}, ARRAY_FILTER_USE_BOTH);
		}
	}
<?php

namespace App\Console\Commands;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class ManualCustomExporter extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exporter:manual';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Manual Get DocID status with DocID array';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Process starting ...');
        $this->manualOveride();
        $this->info('Process ended ...');
    }

    private function manualOveride(): void
	{
		$counter = 0;
    	$this->info('Getting docIDs from local files ...');

    	$docIds = json_decode(file_get_contents('public/docidArray.txt'));

    	$this->info('Decoded into array of docIds ...');
    	$this->info('Initializing call to CCH server ...');
    	foreach($docIds as $docId) {
    		++$counter;

    		$response = (new Client())->request('POST', 'https://access.tourego.com/api/statusById', ['form_params' => ['docId' => $docId]]);

			$customExporterArray = json_decode($response->getBody()->getContents(), true);

			$s3 = Storage::disk('s3');

			$s3File = '/testingRefundRule/' . $docId . '.json';
			//write to S3
			$s3->put($s3File, json_encode($customExporterArray));

			if($counter % 40 === 0) {
				sleep(50);
			}
			var_dump($docId. '        '. $counter);
		}

		$this->info('Saved all files ...');
	}

}

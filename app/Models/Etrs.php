<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Etrs extends Model
{
    protected $table = 'etrs__etrs';

    public function member()
    {
        return $this->belongsTo(Member::class, 'member_id', 'id');
    }

	public function receipts()
	{
		return $this->hasMany(Receipt::class);
	}

}

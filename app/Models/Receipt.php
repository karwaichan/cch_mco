<?php

	namespace App\Models;

	use Illuminate\Database\Eloquent\Model;

	class Receipt extends Model
	{
		protected $table = 'receipt__receipts';

		public function etrs()
		{
			$this->belongsTo(Etrs::class);
		}
	}
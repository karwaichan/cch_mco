<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    protected $table = 'member__members';

    public function etrs()
    {
       return $this->hasMany(Etrs::class, 'member_id', 'id');
    }
}

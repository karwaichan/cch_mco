<?php

	namespace App;

	use App\Models\SystemConfig;
    use Carbon\Carbon;
    use Sabre\Xml\Writer;
    use Sabre\Xml\XmlSerializable;

    class XmlRoot implements XmlSerializable
	{
        private $etrs;
        private $customApproval;
        private $refunded;

        public function __construct($etrs, $customApproval, $refunded)
        {
            $this->etrs = $etrs;
            $this->customApproval = $customApproval;
            $this->refunded = $refunded;
        }

        public function xmlSerialize(Writer $writer)
        {
            $ns = '{http://www.global-blue.com/XMLSchemas/taxRefundTransactions/v1}';
            $tfs = '{http://www.global-blue.com/XMLSchemas/tfs/v1}';

            $writer->writeAttributes([
                'created' => Carbon::now()->toIso8601String(),
                'refundAgencyType' => 'Dtro',
                'refundAgencyId' => '6',
                'receiver' =>'6',
                'serialNumber' => $this->McfFileCounter(),
                'xsi:noNamespaceSchemaLocation' => 'taxRefundTransactions_v1.xsd'
            ]);
            $writer->write([
                [
                    'name' => $ns . 'agent',
                    'attributes' => [
                        'country' => 'SGP',
                        'number' => '1'
                    ],

                    'value' => [
                        'name' => $ns . 'statement',
                        'attributes' => [
                            //TODO:: get mcf running number
                            'number' => '4',
                            'date' => Carbon::now()->toDateString(),
                            'currency' => 'SGD',
                            'content' => 'PAYMENTS'
                        ],

                        'value' => [
                            'name' => $ns . 'traveller',
                            'attributes' => [
                                'name' => $this->etrs->member->first_name . ' ' . $this->etrs->member->last_name,
                                'nationality' => $this->etrs->member->nationality_icao
                            ],

                            'value' => [
                                'name' => $ns . 'cheque',
                                'attributes' => [
                                    'country' => 'SGP',
                                    'currency' => 'SGD',
                                    'issued' => Carbon::parse($this->etrs->created_at)->toIso8601String(),
                                    'docId' => str_replace( '.', '', $this->etrs->doc_id)
                                ],

                                'value' => [[
                                                'name' => $ns . 'customsApproval',
                                                'attributes' => [
                                                    'country' => 'SGP'
                                                ],

                                                'value' => [
                                                    'name' => $tfs. 'eCustomsStamp',
                                                    'attributes' => [
                                                        'timestamp' => $this->customApproval->status->stampDateTime,
                                                        'ecsId' => '580000000016539206'
                                                    ]
                                                ]
                                            ],
                                            [
                                                'name' => $ns . 'invoice',
                                                'value' => [
                                                    'name' => $ns . 'amount',
                                                    'attributes' => [
                                                        'vatRate' => '7.00',
                                                        'salesAmount' => (string)$this->etrs->amount,
                                                        'exportAmount' =>
                                                            (string)
                                                            $this->customApproval->status->transactionTotalExportedGrossAmount,
                                                        'gstAmountDuringApproval' => (string)round
														($this->customApproval->status
															 ->transactionTotalExportedGrossAmount * 0.07 /1.07,2)
                                                    ]
                                                ]
                                            ],
                                            [
                                                'name' => $ns . 'refund',
                                                'attributes' => [
                                                    'reversal' => 'false',
                                                    'amount' => (string)$this->refunded->status->refundAmount,
                                                    'time' => Carbon::parse($this->refunded->status->refundDateTime)
                                                        ->toIso8601String()
                                                ],

                                                'value' => [[
                                                                'name' => $tfs . 'paid',
                                                                'attributes' => [
                                                                    'amount' => (string)
                                                                    $this->refunded->status->refundAmount
                                                                ]
                                                            ],
                                                            [
                                                                'name' => $tfs . 'commission',
                                                                'attributes' => [
                                                                    'amount' => '0.00'
                                                                ]
                                                            ]
                                                ],

                                            ]
                                ]
                            ]
                        ]
                    ]
                ],
            ]);
        }

        private function McfFileCounter()
        {
            $config = SystemConfig::where('name', 'mcf_counter')->firstOrFail();
            $currentCounter = $config->value;

            $config->value += 1;
            $config->update();

            return $currentCounter;
        }
    }

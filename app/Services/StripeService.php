<?php

    namespace App\Services;

    use App\Exceptions\GeneralException;
    use App\Exceptions\StripeException;
    use Psy\Util\Str;
    use Stripe\PaymentIntent;
    use Stripe\PaymentMethod;
    use Stripe\Stripe;
    use Stripe\StripeClient;

    class StripeService
    {

        /**
         * @var StripeClient
         */
        private $stripe;

        public function __construct()
        {
            $this->stripe = new StripeClient(config('constants.STRIPE_SECRET_KEY'));
        }

        public function createPaymentIntent(PaymentMethod $paymentMethod)
        {
            $this->stripe->paymentIntents->create([
                'payment_method'      => $paymentMethod->toArray()['id'],
                'amount'              => 9999,
                'currency'            => 'sgd',
                'confirmation_method' => 'manual',
                'capture_method' => 'manual',
                'payment_method_options' => [
                    'card_present' => ['request_extended_authorization' => true],
                ],
            ]);
        }

        public function captureFundFromPaymentIntent()
        {
            try{
                $paymentIntent = $this->stripe->paymentIntents->retrieve('pi_3MPhXGATeDEpIfve0cvSF3dF');
                $paymentIntent->confirm();
            }catch(\Exception $exception) {
                throw new StripeException($exception);
            }

        }

        /**
         * @throws StripeException
         */
        public function createPaymentMethod(): \Stripe\PaymentMethod
        {
            try {
                return $this->stripe->paymentMethods->create([
                    'type' => 'card',
                    'card' => [
                        //TODO::get real credit card detail
                        'number'    => '4242424242424242',
                        'exp_month' => 8,
                        'exp_year'  => 2023,
                        'cvc'       => '314',
                    ]
                ]);
            }
            catch (\Exception $exception) {
                throw new StripeException($exception);
            }

        }

        public function cancelPreauth()
        {
            try {
                return $this->stripe->paymentIntents->cancel('pi_3MPhYkATeDEpIfve3iKQAq8n');
            }catch(\Exception $exception) {
                throw new StripeException($exception);
            }
        }
    }

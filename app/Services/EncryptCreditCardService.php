<?php

	namespace App\Services;

	class EncryptCreditCardService
	{

        public function encryptCreditCard($creditNumber=NULL)
        {
            $pan = '';
            openssl_public_encrypt($creditNumber, $pan, $this->creditcardEncryptionKey());
            $encryptedCreditCardPan = bin2hex($pan);
            return $encryptedCreditCardPan;

        }

        private function creditcardEncryptionKey()
        {
            return '-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoArrn9hWdGG8iKu/la0+
TAibxn5chUrm2S2Snz4PSG+d345uF+Ejx2F5wLAFwCPAl0GNuoKHbXd1D3V9GlUY
OWH3GmTOxS4wPAOmIXADrOM8CHkOH1zycKGMN5jaaMjNBbXDMx6J+d5a5ef9Euog
bCXoLrDcRXHL5D9d4eAq4MPBGD55gaM5jUawB6URgZWgBbMxUBvAfjyzX8qCAo70
hXYmA3EYMGptX7ZDMHyVFoOrbfYPNPiHvigMSh1RKpCozVlNzOjvT2K96+Gu4+/l
rSIXHr24/PEX7Nbv1MMgfObCCZKFbjZoq1+GMuxeFDpChSAR1jxsHLibWHMbkuxC
gwIDAQAB
-----END PUBLIC KEY-----';
        }

        private function creditcardEncryptionKeyId()
        {
            return 'cch-rsa-uat-dtro-trg2';
        }
	}

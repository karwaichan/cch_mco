<?php

	namespace App\Services;

	use App\Exceptions\DtroException;
    use GuzzleHttp\Client;
    use Illuminate\Support\Str;

    class BinlistService
	{
        private $cardNumber;

        public function __construct($cardNumber)
        {
            $this->cardNumber = $cardNumber;
        }

        public function check()
        {
            $splicedCardNumber = $this->spliceCardNumberToLookupRange();

            $this->lookup($splicedCardNumber);;
        }

        /**
         * @throws DtroException
         */
        private function lookup($cardNumber)
        {
            try{
                $client = new Client();

                $response = $client->request('GET', 'https://bin-lookup3.p.rapidapi.com/', $this->headerOptions($cardNumber));
                dd($response->getBody()->getContents());

            }catch(\Exception $exception) {dd($exception);
                throw new DtroException($exception);
            }
        }

        private function headerOptions($cardNumber)
        {
            return [
                'headers' => [
                    'Content-Type' => 'application/json;charset=utf-8',
                    'Accept' => 'application/json',
                    'Cache-Control' => 'no-cache',
                    'X-RapidAPI-Host'=> 'findbinnumbers.p.rapidapi.com',
                    'X-RapidAPI-Key'=> '3ef6cb37eemshcb3fad5ad8bbb56p16f903jsn8b716afa1846',
                ],

                'query' => $cardNumber,
            ];
        }

        private function spliceCardNumberToLookupRange()
        {
            $card = str_replace(' ', '', $this->cardNumber);

            return substr($card, '0', '-8');
        }
	}

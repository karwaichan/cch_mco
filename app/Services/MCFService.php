<?php

	namespace App\Services;

	use App\Models\Etrs;
    use App\Models\Member;
    use App\ValueObjects\AccessToken;
    use App\ValueObjects\JWT;
    use App\XmlRoot;
    use Carbon\Carbon;
    use GuzzleHttp\Client;
    use http\Client\Request;
    use Illuminate\Support\Collection;
    use Illuminate\Support\Facades\URL;
    use Sabre\Xml\Service;

    class MCFService
	{

        /**
         * @var Client
         */
        private $client;

        public function __construct(Client $client)
        {
            $this->client = $client;
        }

        public function generate($statusResult)
        {
            $docId = request()->get('docId');
            //retrieve etrs info
            $etrs = Etrs::where('doc_id', $docId)->firstOrFail();
            //get custom stamp info
            $collection = collect($statusResult);
            $customApproval = $this->customApprovalObject($collection);

            $refunded = $this->refundedObject($collection);

            return $this->generateXML($etrs, $customApproval, $refunded);

        }

        private function customApprovalObject(Collection $collection)
        {
           return $collection->filter(function($value){
                if(isset($value->status->customsStatus)) {
                    return $value->status->customsStatus === 'Approved';
                }
            })->first();
        }

        private function refundedObject(Collection $collection)
        {
            return $collection->filter(function($value){
                if(isset($value->status->refundStatus)) {
                    return $value->status->refundStatus === 'RefundExecuted';
                }
            })->first();
        }

        private function generateXML(Etrs $etrs, $customApproval, $refunded)
        {
            $service = new Service();

            $ns = '{http://www.global-blue.com/XMLSchemas/taxRefundTransactions/v1}';
            $tfs = '{http://www.global-blue.com/XMLSchemas/tfs/v1}';

            $service->namespaceMap = [
                'http://www.global-blue.com/XMLSchemas/common/v1' => 'common',
                'http://www.global-blue.com/XMLSchemas/tfs/v1' => 'tfs',
                'http://www.global-blue.com/XMLSchemas/taxRefundTransactions/v1' => 'trt',
                'http://www.w3.org/2001/XMLSchema-instance' => 'xsi'
            ];

            $xml = new XmlRoot($etrs, $customApproval, $refunded);
           echo $service->write($ns . 'taxRefundTransactions', $xml);

        }
	}

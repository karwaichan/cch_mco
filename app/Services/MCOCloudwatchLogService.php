<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 25/2/2020
	 * Time: 1:17 PM
	 */

	namespace App\Services;


	use Aws\CloudWatchLogs\CloudWatchLogsClient;
    use Illuminate\Support\Carbon;
    use Illuminate\Support\Collection;
    use Illuminate\Support\Facades\Storage;

    /**
	 * @property CloudWatchLogsClient cloudwatchClient
	 */
	class MCOCloudwatchLogService
	{
        /**
         * MCO start on 27 December 2019
         * @var float|int
         */
        private $startDate;
        /**
         * @var float|int
         */
        private $endDate;

        const SEARCH = 'SEARCH';
        const STAMP = 'STAMP';

        public function __construct()
		{
			$this->cloudwatchClient = new CloudWatchLogsClient([
																   'region'      => env('AWS_DEFAULT_REGION'),
																   'version'     => 'latest',
																   'credentials' => [
																	   'key'    => env('AWS_ACCESS_KEY_ID'),
																	   'secret' => env('AWS_SECRET_ACCESS_KEY')
																   ]
															   ]);

			$this->startDate = Carbon::parse('2019-12-27')->startOfDay()->timestamp * 1000;
			$this->endDate = Carbon::now()->endOfDay()->timestamp * 1000;
		}

		public function retrieveMCOCloudwatchLogs()
		{

			$searchTransactionCollection = $this->getCleanTransaction(self::SEARCH);

			if($searchTransactionCollection->isEmpty() === false) {
                $this->saveToFile($searchTransactionCollection, self::SEARCH);
            }

			$stampTransactionCollection = $this->getCleanTransaction(self::STAMP);

			if($stampTransactionCollection->isEmpty() === false) {
			    $this->saveToFile($stampTransactionCollection, self::STAMP);
            }


		}

		private function getCleanTransaction($method)
		{
			$unfilteredLogs = [];

			$nextToken = null;

			if($method === 'SEARCH') {
                do {
                    $logs             = $this->getSearchCloudwatchLogByPeriod($nextToken);
                    $unfilteredLogs[] = $logs;
                    $nextToken        = $logs['nextToken'] ?? null;
                } while ($nextToken !== null);

                //TODO:: what if the logs is empty
                if (!empty($logs)) {
                    return $this->processedUnfilteredSearchLogs($unfilteredLogs);
                }
            }

			if($method === 'STAMP'){
                do {
                    $logs             = $this->getStampCloudwatchLogByPeriod($nextToken);
                    $unfilteredLogs[] = $logs;
                    $nextToken        = $logs['nextToken'] ?? null;
                } while ($nextToken !== null);

                //TODO:: what if the logs is empty
                if (!empty($logs)) {
                    return $this->processedUnfilteredStampLogs($unfilteredLogs);
                }
            }


			return response()->json(['message' => 'No data.']);
		}

		/**
		 * Retrieve all the log for the particular month
		 *
		 * If that month has too many request, set $date and iterate till $date is less than last day of the month
		 *
		 * @param null $date
		 */
		private function getSearchCloudwatchLogByPeriod($nextToken = null)
		{
			$filterCriteria = [
				'endTime'        => $this->endDate,
				'filterPattern'  => '"production.NOTICE: api/search"',
				'logGroupName'   => 'ETRS2.0',
				'logStreamNames' => ['CCH2'],
				'startTime'      => $this->startDate
			];

			if($nextToken !== null) {$filterCriteria['nextToken'] = $nextToken;}

			return $this->cloudwatchClient->filterLogEvents($filterCriteria)->toArray();

		}

        private function getStampCloudwatchLogByPeriod($nextToken = null)
        {
            $filterCriteria = [
                'endTime'        => $this->endDate,
                'filterPattern'  => '"production.INFO: api/stamp"',
                'logGroupName'   => 'ETRS2.0',
                'logStreamNames' => ['CCH2'],
                'startTime'      => $this->startDate
            ];

            if($nextToken !== null) {$filterCriteria['nextToken'] = $nextToken;}

            return $this->cloudwatchClient->filterLogEvents($filterCriteria)->toArray();
		}

        private function processedUnfilteredStampLogs(array $logs): Collection
        {
            $eventCollection = collect($logs);

            return $eventCollection->pluck('events')->collapse()->map(function($value){
                return ['timestamp' => Carbon::createFromTimestamp($value['timestamp'] / 1000)->toDateTimeString(), 'message' => $this->stripeBracketFromLog($value['message'])];
            });
		}

        private function stripeBracketFromLog($message)
        {
            $readableString = preg_replace("/\r|\n/", '', $message);
            $findStringPositionToCut = strpos($readableString, '{');
            $jsonEncodedLog = substr($message, $findStringPositionToCut);
            return json_decode($jsonEncodedLog, true);
		}

		private function processedUnfilteredSearchLogs(array $logs)
		{

			$eventCollection = collect($logs);

			$logs = $eventCollection->pluck('events')->collapse()->pluck('message')->transform(function($value){
				return preg_replace("/\r|\n/", '', $value);
			});

			$jsonDecodedLogs = $logs->transform(function($value){
				$findStringPositionToCut = strpos($value, '{');
				$jsonEncodedLog = substr($value, $findStringPositionToCut);
				return json_decode($jsonEncodedLog, true);
			});

			$notEmptyTransactionLogs = $jsonDecodedLogs->filter(function($value){
			    return !empty($value['message']['transactions']);
			});

			$uniqueLogs = $notEmptyTransactionLogs->unique();

			$finalLogs =  $uniqueLogs->pluck('message')->transform(function ($value) {
                $value['transactions'] = collect($value['transactions'])->map(function ($value2) use($value) {
                    $value2['passportNumber'] = $value['consolidatedTraveler']['passportNumber'];
                    return $value2;
                });
                return $value;
            });

			return $finalLogs;
		}

		private function saveToFile($data, $keyword)
		{
			$s3 = Storage::disk('s3');

			switch ($keyword) {
                case self::SEARCH:
                    $s3File = '/etrs2.0Logs/searchData.json';
                    break;
                case self::STAMP:
                    $s3File = '/etrs2.0Logs/stampData.json';
                    break;
            }

			$s3->put($s3File, $data);
		}

	}

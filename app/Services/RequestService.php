<?php

	namespace App\Services;

	use App\Exceptions\ConnectionException as OwnConnectionException;
	use App\Exceptions\GeneralException;
	use App\Exceptions\McoException;
	use App\Interfaces\IRASRequestInterface;
	use App\ValueObjects\AccessToken;
	use App\ValueObjects\EtrsTransaction\EtrsStatusBatch;
	use App\ValueObjects\EtrsTransaction\MCOGetAlipayAccount;
	use App\ValueObjects\EtrsTransaction\McoLanguage;
	use App\ValueObjects\JWT;
	use App\ValueObjects\MCOAcessToken;
	use Carbon\Carbon;
	use Exception;
	use GuzzleHttp\Client;
	use GuzzleHttp\Exception\ClientException;
	use GuzzleHttp\Exception\ConnectException;
	use Psr\Http\Message\ResponseInterface;

	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 13/9/2018
	 * Time: 3:28 PM
	 */

	/**
	 * Class RequestService
	 * @package App\Services
	 */
	class RequestService
	{
		/**
		 * @var Client
		 */
		private $client;

		public function __construct(Client $client)
		{
			$this->client = $client;
		}

		/**
		 * @param $requester
		 * @return Exception|string
		 */
		public function sendRequest($requester)
		{
			//Check access token validity
			$accessToken = $this->isValidToken();

			//if access token is an exception, return exception
			if ($accessToken instanceof Exception) {
				throw new GeneralException($accessToken);
			}

			if ($requester instanceof EtrsStatusBatch) {
				return $this->getRequest($requester);
			}
			return $this->postRequest($requester);
		}

		public function mcoRequest($requester)
		{
			$accessToken = $this->isValidMcoToken();

			if ($requester instanceof McoLanguage) {
				return $this->putRequest($requester);
			}

			if($requester instanceof MCOGetAlipayAccount){
				return $this->getRequest($requester);
			}

			return $this->postRequest($requester);
		}

		private function isValidMcoToken()
		{
			//If access token file exists, get file content
			if (file_exists(config('constants.MCO_ACCESS_TOKEN_FILE'))) {
				$accessToken = json_decode(file_get_contents(config('constants.MCO_ACCESS_TOKEN_FILE')));
			}
			//set the endTime if exists and null if otherwise
			$endTime = empty($accessToken) ? null : Carbon::parse($accessToken->end_time);

			//if endTime is null or NOW > endTime, request new access token
			if ($endTime === null || Carbon::now()->gt($endTime)) {
				//request a new token
				$accessToken =  $this->postRequest(new MCOAcessToken(new JWT()));

				//if accessToken is not exception, save accessToken to temporary file
				if(!($accessToken instanceof Exception)) {
					$this->saveMcoAccessToken($accessToken);
				}
			}
			return $accessToken;
		}

		/**
		 * Verify access token validity
		 * > If endTime of accessToken more than 5 minutes, request new accessToken
		 *
		 * @param null $accessToken
		 * @return Exception|string
		 */
		private function isValidToken($accessToken = null)
		{
			//If access token file exists, get file content
			if (file_exists(config('constants.ACCESS_TOKEN_FILE'))) {
				$accessToken = json_decode(file_get_contents(config('constants.ACCESS_TOKEN_FILE')));
			}
			//set the endTime if exists and null if otherwise
			$endTime = empty($accessToken) ? null : Carbon::parse($accessToken->end_time);

			//if endTime is null or NOW > endTime, request new access token
			if ($endTime === null || Carbon::now()->gt($endTime)) {
				//request a new token
				$accessToken =  $this->postRequest(new AccessToken(new JWT()));

				//if accessToken is not exception, save accessToken to temporary file
				if(!($accessToken instanceof Exception)) {
					$this->saveAccessToken($accessToken);
				}
			}
			return $accessToken;
		}

		/**
		 * Save accessToken to temporary files for further API references
		 *
		 * @param $accessToken
		 * @return bool|int
		 */
		private function saveAccessToken(ResponseInterface $response)
		{
			$decodedAccessToken = (array)json_decode($response->getBody()->getContents());
			$decodedAccessToken = json_encode(array_merge($decodedAccessToken, ['end_time' => Carbon::now()->addMinutes(5)->toDateTimeString()]));

			return file_put_contents(config('constants.ACCESS_TOKEN_FILE'), $decodedAccessToken);
		}

		/**
		 * Save accessToken to temporary files for further API references
		 *
		 * @param $accessToken
		 * @return bool|int
		 */
		private function saveMcoAccessToken(ResponseInterface $response)
		{
			$decodedAccessToken = (array)json_decode($response->getBody()->getContents());
			$decodedAccessToken = json_encode(array_merge($decodedAccessToken, ['end_time' => Carbon::now()->addMinutes(4)->toDateTimeString()]));

			return file_put_contents(config('constants.MCO_ACCESS_TOKEN_FILE'), $decodedAccessToken);
		}

		private function postRequest(IRASRequestInterface $request)
		{
			try{
				return $this->client->request('POST', $request->requestURL(), $request->headerOptions());
			}catch(Exception $exception){
				$this->selectException($exception);
			}
		}

		private function getRequest(IRASRequestInterface $request)
		{
			try {
				return $this->client->request('GET', $request->requestURL(), $request->headerOptions());
			}catch (Exception $exception) {
				$this->selectException($exception);
			}
		}

		private function putRequest(IRASRequestInterface $request)
		{
			try{
				return $this->client->request('PUT', $request->requestURL(), $request->headerOptions());
			}
			catch(Exception $exception){
				$this->selectException($exception);
			}
		}

		//TODO:: Throttle put in generalexception for now. Wait until UAT testing only put accordingly
		private function selectException(Exception $exception)
		{
			switch ($exception) {
				case $exception instanceOf ConnectException:
					throw new OwnConnectionException($exception);
					break;
				case $exception instanceOf ClientException:
					throw new McoException($exception);
					break;
				default:
					throw new GeneralException($exception);
			}
		}
	}

<?php

	namespace App\Services;

	use App\ValueObjects\JWT;
	use Carbon\Carbon;
	use Illuminate\Http\JsonResponse;

	/**
	 * @property JWT jwtObject
	 * @property RequestService requestService
	 */
	class AccessTokenService
	{
		/**
		 * @var JWT
		 */
		private $jwtObject;
		/**
		 * @var RequestService
		 */
		private $requestService;

		public function __construct(JWT $JWT, RequestService $requestService)
		{
			$this->jwtObject      = $JWT;
			$this->requestService = $requestService;
		}

		/**
		 * @return JsonResponse
		 */
		public function initializeRequest(): JsonResponse
		{
			//Get access token
			$accessToken = $this->requestService->sendRequest($this);
			//save access token to a tmp folder
			$this->saveAccessToken($accessToken);
		}

		/**
		 * Save access token to temporary file
		 * Business Logic :
		 * 1. Request access token every 5 minutes (Truth: token only invalid if no activity within 5 minutes and lifespan of the token more than 1 hour)
		 * 2. Push end_time in access token for later validation check
		 *
		 * @param $accessToken
		 * @return bool|int
		 */
		private function saveAccessToken($accessToken)
		{
			$decodedAccessToken = (array)json_decode($accessToken);
			$decodedAccessToken = json_encode(array_merge($decodedAccessToken, ['end_time' => Carbon::now()->addMinutes(5)->toDateTimeString()]));

			$file = '/tmp/accessToken.txt';

			return file_put_contents($file, $decodedAccessToken);
		}
	}

<?php


	namespace App\Services;


	use App\Models\Models\Interfaces\IRASRequestInterface;
    use App\Models\Models\ValueObjects\AccessToken;
    use App\Models\Models\ValueObjects\JWT;
    use Carbon\Carbon;
    use GuzzleHttp\Client;
    use http\Exception;
    use Psr\Http\Message\ResponseInterface;

    class CheckVoidTransactionService
	{

        public function __construct(Client $client)
        {
            $this->client = $client;

	    }
        public function checkforNotTallyTransaction()
        {
            #get records from CSV file.
            $rawRecords = collect($this->getRecordArray());
            #minimize to small chunk of array
            $smallerChunk = $rawRecords->transform(function($value){
                if($value !== false){
                    $collection = collect($value);
                    $filterCollection = $collection->only([0,1,7]);
                    $filterCollection[7] = str_replace('.', '', $filterCollection[7]);
                    return $filterCollection;
                }
            });

            $finalChunk = $smallerChunk->filter(function($value){
                return $value !== null;
            });

            $refundedTicket = [];
            $counter = 0;
            #call get statusbyId
            $result = $finalChunk->transform(function($value, $key)use($refundedTicket){

                if ($key % 100 === 0 && $key !== 0) {
                        sleep(70);
                    }

                try {
                    $result = $this->callAPI($value[7]);

                    $collectionResult = collect(json_decode($result->getBody()->getContents(), true));

                    $allStatus = $collectionResult->pluck('status')->pluck('status');

                    #check if refunded exist
                    if ($allStatus->contains('Refund')) {
                        $refundedTicket[] = $value[7];
                    }

                    return $refundedTicket;
                }
                catch (\Exception $exception) {var_dump($value[7]);}
            });

            dd($result->collapse());
	    }


        private function getRecordArray()
        {
            $csvFile  = fopen(storage_path("/voided/voided_sep_2019.csv"), 'r');

            while(!feof($csvFile)) {
                $recordArray[] = fgetcsv($csvFile, 0, ',');
            }
            fclose($csvFile);
            return $recordArray;
	    }

        private function callAPI($docid)
        {
            #get accesstoken
            //If access token file exists, get file content
            if (file_exists(config('constants.ACCESS_TOKEN_FILE'))) {
                $accessToken = json_decode(file_get_contents(config('constants.ACCESS_TOKEN_FILE')));
            }
            //set the endTime if exists and null if otherwise
            $endTime = empty($accessToken) ? null : Carbon::parse($accessToken->end_time);

            //if endTime is null or NOW > endTime, request new access token
            if ($endTime === null || Carbon::now()->gt($endTime)) {
                //request a new token
                $accessToken =  $this->postRequest(new AccessToken(new JWT()));

                //if accessToken is not exception, save accessToken to temporary file
                if(!($accessToken instanceof Exception)) {
                    $this->saveAccessToken($accessToken);
                }
            }

            #call statusbyID
            return $this->client->request('POST', config('constants.GET_STATUS_BY_ID_URL'), $this->requestHeader($docid));


	    }

        private function requestHeader($docid)
        {
            $jsonString = ['docId' => $docid];
            return [
                'base_uri' => config('constants.METHOD_BASE_URL'),
                'verify'   => config('constants.CERTIFICATE'),
                //TODO::set back to 5 when production, UAT server lower processing power
                'timeout'  => 30.0,
                'headers'  => [
                    'Content-Type'  => 'application/json',
                    'Accept' => 'application/json',
                    'Authorization' => $this->getAccessToken(),
                    'Cache-Control' => 'no-cache'
                ],
                'body' => json_encode($jsonString)
            ];
	    }

        protected function getAccessToken(): string
        {
            $accessToken      = json_decode(file_get_contents(config('constants.ACCESS_TOKEN_FILE')));
            $authorizationKey = 'Bearer ' . $accessToken->access_token;

            return $authorizationKey;
        }


        private function postRequest(IRASRequestInterface $request)
        {
            try{
                return $this->client->request('POST', $request->requestURL(), $request->headerOptions());
            }catch(\Exception $exception){
                $this->selectException($exception);
            }
        }

        private function saveAccessToken(ResponseInterface $response)
        {
            $decodedAccessToken = (array)json_decode($response->getBody()->getContents());
            $decodedAccessToken = json_encode(array_merge($decodedAccessToken, ['end_time' => Carbon::now()->addMinutes(5)->toDateTimeString()]));

            return file_put_contents(config('constants.ACCESS_TOKEN_FILE'), $decodedAccessToken);
        }

	}

<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 19/6/2019
	 * Time: 1:15 PM
	 */

	//REQUEST BODIES
	/**
	 * @OA\RequestBody(
	 *     request = "statusById",
	 *     description = "eTRS doc Id to retrieve",
	 *     required = true,
	 *     @OA\JsonContent(ref="#/components/schemas/statusById")
	 * )
	 */

	/**
	 * @OA\RequestBody(
	 *     request = "statusByPeriod",
	 *     description = "Date period as end date for retrieval",
	 *     required = true,
	 *     @OA\JsonContent(ref="#/components/schemas/statusByPeriod")
	 * )
	 */

	/**
	 * @OA\RequestBody(
	 *     request = "void",
	 *     description = "eTRS doc ID to be voided",
	 *     required = true,
	 *
	 *     @OA\JsonContent(ref="#/components/schemas/void")
	 * )
	 */

	/**
	 * @OA\RequestBody(
	 *     request="issue",
	 *     description = "eTRS details to be created",
	 *     required = true,
	 *
	 *     @OA\JsonContent(ref="#/components/schemas/issue")
	 * )
	 */


	//SCHEMA

	/**
	 * @OA\Schema(
	 *    schema = "statusById",
	 *     @OA\Property(property="docId", type="string", example = "20062100000000030671"),
	 *    ),
	 */

	/**
	 * @OA\Schema(
	 *     schema="statusByPeriod",
	 *     @OA\Property(property="DateTime", type="string", format="date-time", example="2019-06-05 15:23:05")
	 * )
	 */

	/**
	 * @OA\Schema(
	 *     schema = "void",
	 *     @OA\Property(property="docId", type="string", example = "20062100000000030754")
	 * )
	 */

	/**
	 * @OA\Schema(
	 *     schema = "shop_items",
	 *
	 *     @OA\Property(
	 *     		property = "id",
	 *     		type = "integer",
	 *     		format = "int64",
	 *     		example = 97444580
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "name1",
	 *     		type = "string",
	 * 			example = "Waters, Gerlach and Heller",
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "name2",
	 *     		type = "string",
	 *     		example = "Inc"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "address1",
	 *     		type = "string",
	 *     		example = "Anne Squares"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "address2",
	 *     		type = "string",
	 *     		example = "4061 Bailey Fort"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "postalCode",
	 *     		type = "string",
	 *     		example = "32150"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "gstRegistrationNumber",
	 *     		type = "string",
	 *     		example = "G356673594"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "activationDate",
	 *     		type = "string",
	 *     		format = "date",
	 *     		example ="2019-04-04"
	 *     )
	 * )
	 */

	/**
	 * @OA\Schema(
	 *     schema = "traveller_items",
	 *
	 *     @OA\Property(
	 *     		property = "passportNumber",
	 *     		type = "string",
	 *     		example = "TEST"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "nationality",
	 *     		type = "string",
	 *     		example = "ABW"
	 *     ),
	 *
	 *
	 *     @OA\Property(
	 *     		property = "dateOfBirth",
	 *     		type = "string",
	 *     		format = "date",
	 *     		example = "1987-07-15"
	 *     ),
	 * )
	 */

	/**
	 * @OA\Schema(
	 *     schema = "purchase_items",
	 *
	 *     @OA\Property(
	 *     		property = "receiptNumber",
	 *     		type = "string",
	 *     		example = "TOU_test2263"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "receiptDate",
	 *     		type = "string",
	 *     		format = "datetime",
	 *     		example = "2019-10-09T08:37:17"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "gstRate",
	 *     		type = "number",
	 *     		format = "double",
	 *     		example = 7
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "description",
	 *     		type = "string",
	 *     		example = "Example description"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "detailDescription",
	 *     		type = "string",
	 *     		example = "Detail Description Example"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "itemGrossAmount",
	 *     		type = "number",
	 *     		format = "double",
	 *     		example = 200
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "itemQuantity",
	 *     		type = "integer",
	 *     		format = "int64",
	 *     		minimum = 1,
	 *     		example = 1
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "goodsCategory",
	 *     		type = "string",
	 *     		example = "Fashion & Clothing"
	 *     )
	 * )
	 */

	/**
	 * @OA\Schema(
	 *     schema = "issue",
	 *
	 *     @OA\Property(
	 *        property="docId",
	 *        type="string",
	 *        example = "20062171103000002098"
	 *        ),
	 * 		@OA\Property(
	 *            property = "refundRule",
	 *            type = "integer",
	 *            example = 21
	 *        ),
	 *     	@OA\Property(
	 *     			property = "issueDateTime",
	 *     			type = "datetime",
	 *     			example = "2019-10-09T08:37:17"
	 *        ),
	 *     @OA\Property(
	 *     		property = "provisionalRefundAmount",
	 *     		type= "number",
	 *     		format = "double",
	 *     		minimum = 0.01,
	 *     		example = 5
	 *     ),
	 *     @OA\Property(
	 *     		property = "serviceFeeAmount",
	 *     		type = "number",
	 *     		format = "double",
	 *     		minimum = 0.01,
	 *     		example = 1.54
	 *     ),
	 *     @OA\Property(
	 *     		property = "shop",
	 *     		ref="#/components/schemas/shop_items"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "traveller",
	 *     		ref = "#/components/schemas/traveller_items"
	 *     ),
	 *
	 *     @OA\Property(
	 *     		property = "purchaseDetails",
	 *     		@OA\Items(ref = "#/components/schemas/purchase_items")
	 *     ),
	 * )
	 */

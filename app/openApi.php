<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 14/6/2019
	 * Time: 3:36 PM
	 */

	/**
	 * @OA\Info(
	 *     description = "API documentation for CCH2 integration",
	 *     version="1.0.0",
	 *     title = "CCH2 API",
	 *     @OA\Contact(
	 *            name = "API support",
	 *            email = "karwai@tourego.com.sg"
	 *     ),
	 * )
	 */

	/**
	 * @OA\Tag(
	 *     name="etrs2.0",
	 *     description = "Every APIs regarding eTRS 2.0"
	 * )
	 */

	/**
	 * @OA\Server(
	 *     description = "CCH2 UAT Server",
	 *     url = "http://18.141.167.141/"
	 * )
	 */

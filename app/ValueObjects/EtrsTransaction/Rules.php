<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 21/9/2018
	 * Time: 4:27 PM
	 */

	namespace App\ValueObjects\EtrsTransaction;


	/**
	 * Class Rules
	 * @package App\ValueObjects\newTransaction
	 *
	 * Validation rules to validate newTransaction CCH2.0 from Backend api
	 */
	final class Rules
	{
		/**
		 * Validation rules for issuing a new transaction
		 * @return array
		 */
		public static function newTransaction(): array
		{
			return [
				'docId'                      => 'required|string|max:20',
				'refundRule'                 => 'required|digits_between:1,2',
				'issueDateTime'              => 'required|string| max:25',
				'provisionalRefundAmount'    => 'required|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/|max:16',
				'serviceFeeAmount'           => 'required|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/|max:16',
				'shop.id'                    => 'required|digits_between:1,8',
				'shop.name1'                 => 'required|string|max:50',
				'shop.name2'                 => 'string|max:50',
				'shop.address1'              => 'string|max:50',
				'shop.address2'              => 'string|max:50',
				'shop.postalCode'            => 'required|string|max:6',
				'shop.gstRegistrationNumber' => 'required|string|max:50',
				'shop.activationDate'        => 'required|string|date_format:Y-m-d| max:10',
				'traveller.passportNumber'   => 'required|string|max:20',
				'traveller.nationality'      => 'required|string|max:3',
				'traveller.dateOfBirth'      => 'string|date_format:Y-m-d|max:10',
				'purchaseDetails'            => 'required|array'
			];
		}

		/**
		 * Combination from newTransaction to validate purchase details
		 * because purchaseDetails may contain more than one receipts
		 * @return array
		 */
		public static function purchaseDetail(): array
		{
			return [
				'receiptNumber'     => 'required|string|max:20',
				'receiptDate'       => 'required|string|date_format:Y-m-d\TH:i:s|max:25',
				'gstRate'           => 'required|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/|max:16',
				'description'       => 'string|max:50',
				'detailDescription' => 'string|max:100',
				'itemGrossAmount'   => 'required|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/|max:16',
				'itemQuantity'      => 'digits_between:1,5',
				'goodsCategory'     => 'required|string|max:100'
			];
		}

		/**
		 * Rules for void an transaction
		 * @return array
		 */
		public static function voidTransaction(): array
		{
			return [
				'docId' => 'required|string|max:20'
			];
		}

		/**
		 * Rules to get status by period
		 * @return array
		 */
		public static function statusByPeriod(): array
		{
			return [
				'DateTime' => 'required|max:25|date_format:Y-m-d H:i:s'
			];
		}

		/**
		 * Rules to get status by single id
		 * @return array
		 */
		public static function statusById(): array
		{
			return [
				'docId' => 'required|string|max:20'
			];
		}
	}
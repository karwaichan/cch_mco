<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 14/1/2020
	 * Time: 5:29 PM
	 */

	namespace App\ValueObjects\EtrsTransaction;


	use App\Interfaces\IRASRequestInterface;

	class MCOGetAlipayAccount extends AbstractEtrs implements IRASRequestInterface
	{
		public function requestUrl():string
		{
			return config('constants.ALIPAY_URL'). '/' . request('logon_id');
		}

		protected function getAccessToken(): string
		{
			$accessToken      = json_decode(file_get_contents(config('constants.MCO_ACCESS_TOKEN_FILE')));
			$authorizationKey = 'Bearer ' . $accessToken->access_token;

			return $authorizationKey;
		}
	}
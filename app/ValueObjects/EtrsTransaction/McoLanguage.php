<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 19/9/2019
	 * Time: 1:51 PM
	 */

	namespace App\ValueObjects\EtrsTransaction;


	use App\Interfaces\IRASRequestInterface;

	class McoLanguage extends AbstractEtrs implements IRASRequestInterface
	{

		public function headerOptions(): array
		{
			return array_merge(parent::headerOptions(), [
				'json' => request()->all()
			]);
		}

		public function requestUrl():string
		{
			return config('constants.MCO_LANGUAGE_URL');
		}

		protected function getAccessToken(): string
		{
			$accessToken      = json_decode(file_get_contents(config('constants.MCO_ACCESS_TOKEN_FILE')));
			$authorizationKey = 'Bearer ' . $accessToken->access_token;

			return $authorizationKey;
		}
	}
<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 19/9/2018
	 * Time: 9:18 AM
	 */

	namespace App\ValueObjects\EtrsTransaction;


	use App\Interfaces\IRASRequestInterface;
	use Illuminate\Http\Request;

	/**
	 * Class newTransaction
	 * @package App\ValueObjects
	 */
	class newTransaction extends AbstractEtrs implements IRASRequestInterface
	{
		/**
		 * @var Request
		 */
		private $request;

		public function __construct(Request $request)
		{
			$this->request = $request;
		}

		/**
		 * @return array
		 */
		public function headerOptions(): array
		{
			return array_merge(parent::headerOptions(), [
				'json' => $this->request->all()
			]);
		}

		/**
		 * Iras API endpoint url
		 * @return string
		 */
		public function requestURL(): string
		{
			return config('constants.ISSUE_TRANSACTION_URL');
		}
	}
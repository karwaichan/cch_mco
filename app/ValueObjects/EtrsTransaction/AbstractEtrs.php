<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 3/10/2018
	 * Time: 3:06 PM
	 */

	namespace App\ValueObjects\EtrsTransaction;


	abstract class AbstractEtrs
	{
		abstract protected function requestUrl();

		/**
		 * Guzzle Http Header options
		 * @return array
		 */
		public function headerOptions(): array
		{
			return [
				'base_uri' => config('constants.METHOD_BASE_URL'),
				'verify'   => config('constants.CERTIFICATE'),
				//TODO::set back to 5 when production, UAT server lower processing power
				'timeout'  => 30.0,
				'headers'  => [
					'Content-Type'  => 'application/json;charset=utf-8',
					'Accept' => 'application/json',
					'Authorization' => $this->getAccessToken(),
					'Cache-Control' => 'no-cache'
				]
			];
		}

		/**
		 * @return string
		 */
		protected function getAccessToken(): string
		{
			$accessToken      = json_decode(file_get_contents(config('constants.ACCESS_TOKEN_FILE')));
			$authorizationKey = 'Bearer ' . $accessToken->access_token;

			return $authorizationKey;
		}
	}

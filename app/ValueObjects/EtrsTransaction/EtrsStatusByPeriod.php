<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 19/9/2018
	 * Time: 4:19 PM
	 */

	namespace App\ValueObjects\EtrsTransaction;


	use App\Interfaces\IRASRequestInterface;
	use Illuminate\Http\Request;

	/**
	 * Class EtrsStatusByPeriod
	 * @property  request
	 * @package App\ValueObjects
	 */
	class EtrsStatusByPeriod extends AbstractEtrs implements IRASRequestInterface
	{
		/**
		 * @var Request
		 */
		private $request;

		public function __construct(Request $request)
		{
			$this->request = $request;
		}

		/**
		 * @return array
		 */
		public function headerOptions(): array
		{
			return array_merge(parent::headerOptions(), [
				'json' => $this->request->all()
			]);
		}

		/**
		 * @return string
		 */
		public function requestURL(): string
		{
			return config('constants.GET_STATUS_BY_PERIOD_URL');
		}
	}
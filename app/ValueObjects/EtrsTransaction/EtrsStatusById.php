<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 20/9/2018
	 * Time: 10:26 AM
	 */

	namespace App\ValueObjects\EtrsTransaction;


	use App\Interfaces\IRASRequestInterface;
	use Illuminate\Http\Request;

	/**
	 * Class EtrsStatusById
	 * @property  request
	 * @package App\ValueObjects
	 */
	class EtrsStatusById extends AbstractEtrs implements IRASRequestInterface
	{
		/**
		 * @var Request
		 */
		private $request;

		public function __construct(Request $request)
		{
			$this->request = $request;
		}

		/**
		 * @return array
		 */
		public function headerOptions(): array
		{
			return array_merge(parent::headerOptions(), [
				'json' => $this->request->all()
			]);
		}

		/**
		 * @return string
		 */
		public function requestURL(): string
		{
			return config('constants.GET_STATUS_BY_ID_URL');
		}
	}

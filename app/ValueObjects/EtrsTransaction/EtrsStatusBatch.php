<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 19/9/2018
	 * Time: 4:02 PM
	 */

	namespace App\ValueObjects\EtrsTransaction;


	use App\Interfaces\IRASRequestInterface;

	/**
	 * Class EtrsStatusBatch
	 * @package App\ValueObjects\EtrsTransaction
	 */
	class EtrsStatusBatch extends AbstractEtrs implements IRASRequestInterface
	{
		/**
		 * @return string
		 */
		public function requestURL(): string
		{
			return config('constants.GET_STATUS_BY_BATCH_URL');
		}
	}
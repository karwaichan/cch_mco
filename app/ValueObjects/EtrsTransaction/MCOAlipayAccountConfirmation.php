<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 16/1/2020
	 * Time: 10:42 AM
	 */

	namespace App\ValueObjects\EtrsTransaction;


	use App\Interfaces\IRASRequestInterface;

	class MCOAlipayAccountConfirmation extends AbstractEtrs implements IRASRequestInterface
	{
		public function headerOptions(): array
		{
			return array_merge(parent::headerOptions(), [
				'json' => request()->except('user_id')
			]);
		}

		public function requestURL(): string
		{
			return config('constants.CONFIRM_ALIPAY_URL');
		}

		protected function getAccessToken(): string
		{
			$accessToken      = json_decode(file_get_contents(config('constants.MCO_ACCESS_TOKEN_FILE')));
			$authorizationKey = 'Bearer ' . $accessToken->access_token;

			return $authorizationKey;
		}
	}
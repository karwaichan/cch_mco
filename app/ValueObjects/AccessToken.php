<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 13/9/2018
	 * Time: 3:57 PM
	 */

	namespace App\ValueObjects;

	use App\Interfaces\IRASRequestInterface;

	/**
	 * Class AccessToken
	 * @package App\ValueObjects
	 */
	class AccessToken implements IRASRequestInterface
	{
		/**
		 * @var JWT
		 */
		private $jwt;

		public function __construct(JWT $jwt)
		{
			$this->jwt = $jwt;
		}

		/**
		 * @return array
		 */
		public function headerOptions(): array
		{
			return [
				'base_uri'    => $this->jwt->getBaseApiURL(),
				'verify'      => config('constants.CERTIFICATE'),
				//TODO::set back to 5 when production, UAT server lower processing power
				'timeout'     => 30.0,
				'headers'     => [
					'Content-Type'  => 'application/x-www-form-urlencoded',
					'Cache-Control' => 'no-cache'
				],
				'form_params' => [
					'grant_type' => $this->jwt->getGrantType(),
					'assertion'  => $this->jwt->signJWTAssertion()
				]
			];
		}

		/**
		 * Get access token url
		 * @return string
		 */
		public function requestURL(): string
		{
			return config('constants.ACCESS_TOKEN_URL');
		}
	}

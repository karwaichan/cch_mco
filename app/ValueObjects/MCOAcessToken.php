<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 17/9/2019
	 * Time: 1:28 PM
	 */

	namespace App\ValueObjects;


	use App\Interfaces\IRASRequestInterface;

	class MCOAcessToken implements IRASRequestInterface
	{
		/**
		 * @var JWT
		 */
		private $jwt;

		public function __construct(JWT $jwt)
		{
			$this->jwt = $jwt;
		}

		/**
		 * @return array
		 */
		public function headerOptions(): array
		{
			return [
				'base_uri'    => $this->jwt->getBaseApiURL(),
				'verify'      => config('constants.CERTIFICATE'),
				'timeout'     => 30.0,
				'headers'     => [
					'Content-Type'  => 'application/x-www-form-urlencoded',
					'Cache-Control' => 'no-cache'
				],
				'form_params' => [
					'grant_type' => $this->jwt->getGrantType(),
					'assertion'  => $this->jwt->signMcoJWTAssertion()
				]
			];
		}

		/**
		 * Iras API endpoint url
		 *
		 * @return mixed
		 */
		public function requestURL(): string
		{
			return config('constants.ACCESS_TOKEN_URL');
		}
	}

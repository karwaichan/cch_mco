<?php

	namespace App\ValueObjects;

	use Carbon\Carbon;
	use Firebase\JWT\JWT as firebaseJWT;

	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 13/9/2018
	 * Time: 1:38 PM
	 */

	/**
	 * Class JWT
	 * @package App\ValueObjects
	 */
	final class JWT
	{
		/**
		 * 1. Issuer (iss), the issuer is the Client ID issued by API Gateway
		 * 2. Audience (aud), API Gateway token endpoint address. The value is fixed to “http://apiserver/api/oauth/token”
		 * 3. Validity (exp), the expiration time of the assertion, the default value is 5 mins, expressed as the number of seconds from 1970-01-01T0:0:0Z measured in SGT
		 * 4. Time (iat), the time of assertion was issued, measured in seconds after 00:00:00 SGT, January 1, 1970
		 */
		/**
		 * @var string
		 */
		private $grantType;
		/**
		 * @var string
		 */
		private $clientId;
		/**
		 * @var string
		 */
		private $audience;
		/**
		 * @var int
		 */
		private $expiryTime;
		/**
		 * @var int
		 */
		private $currentTime;
		/**
		 * @var string
		 */
		private $privateKey;
		/**
		 * @var string
		 */
		private $certificate;
		/**
		 * @var string
		 */
		private $algorithm;
		/**
		 * @var string
		 */
		private $baseApiURL;
		/**
		 * @var string
		 */
		private $mcoClientId;

		/**
		 * @var string
		 */
		private $mcoAudience;

		/**
		 * JWT constructor.
		 */
		public function __construct()
		{
			$this->grantType   = config('jwt.GRANT_TYPE');
			$this->clientId    = config('jwt.CLIENT_ID');
			$this->audience    = config('jwt.AUDIENCE');
			$this->expiryTime  = Carbon::now()->addMinutes(5)->timestamp;
			$this->currentTime = Carbon::now()->timestamp;
			$this->privateKey  = file_get_contents(config('jwt.PRIVATE_KEY'));
			$this->certificate = file_get_contents(config('jwt.CERTIFICATE'));
			$this->algorithm   = config('jwt.ALGORITHM');
			$this->baseApiURL  = config('constants.ACCESS_TOKEN_BASE_URL');
			#mco
			$this->mcoClientId = config('jwt.MCO_CLIENT_ID');
			$this->mcoAudience = config('jwt.MCO_AUDIENCE');
		}

		/**
		 * Sign Jwt Assertion Object with RS246 and encode.
		 * @return string
		 */
		public function signJWTAssertion(): string
		{
			return firebaseJWT::encode($this->jwtAssertionObject(), $this->getPrivateKey(), $this->getAlgorithm());
		}

		public function signMcoJWTAssertion(): string
		{
			return firebaseJWT::encode($this->mcoJwtAssertionObject(), $this->getPrivateKey(), $this->getAlgorithm());
		}

		/**
		 * @return string
		 */
		public function getGrantType(): string
		{
			return $this->grantType;
		}

		/**
		 * @return string
		 */
		public function getClientId(): string
		{
			return $this->clientId;
		}

		/**
		 * @return int
		 */
		public function getExpiryTime(): int
		{
			return $this->expiryTime;
		}

		/**
		 * @return int
		 */
		public function getCurrentTime(): int
		{
			return $this->currentTime;
		}

		/**
		 * @return string
		 */
		public function getAudience(): string
		{
			return $this->audience;
		}

		/**
		 * @return string
		 */
		public function getCertificate(): string
		{
			return $this->certificate;
		}

		/**
		 * @return string
		 */
		public function getPrivateKey(): string
		{
			return $this->privateKey;
		}

		/**
		 * @return string
		 */
		public function getAlgorithm(): string
		{
			return $this->algorithm;
		}

		/**
		 * Return jwt assertion object
		 * @return array
		 */
		private function jwtAssertionObject(): array
		{
			return [
				'iss' => $this->getClientId(),
				'aud' => $this->getAudience(),
				'exp' => $this->getExpiryTime(),
				'iat' => $this->getCurrentTime()
			];
		}

		private function mcoJwtAssertionObject()
		{
			return [
				'iss' => $this->mcoClientId,
				'aud' => $this->audience,
				'exp' => $this->getExpiryTime(),
				'iat' => $this->getCurrentTime()
			];
		}

		/**
		 * @return string
		 */
		public function getBaseApiURL(): string
		{
			return $this->baseApiURL;
		}
	}
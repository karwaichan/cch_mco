<?php

namespace App\ValueObjects\MockData;

use Faker\Factory;

class DtroMockData
{
    public function generateMockAccessTokenResponse(): array
    {
        return [
            'access_token' => 'Xq2Gox4wvW5iLV6lsljCKNMmY6pmA4aZ5jPgpthODhaycw8QtV91Jn',
            'token_type' => 'Bearer',
            'expires_in' => 1200,
            'scope'=> 'SCOPE_DTRO'
        ];
    }

    public function generateMockGetTransactionStatusByDtroIDResponse(): array
    {
        return [
            'provisionalRefundAmount' =>Factory::create()->numberBetween(100, 150),
            'CraIrId' =>7,
            'docId' => Factory::create()->randomNumber(),
            'passportNumber' => Factory::create()->randomNumber(),
            'nationality' => Factory::create()->countryISOAlpha3,
            'touristFirstName' =>Factory::create()->firstName,
            'touristLastName' =>Factory::create()->lastName,
            'transactionIssuedTime' =>Factory::create()->dateTime()->format('c'),
            'transactionTotalGrossAmount' =>Factory::create()->numberBetween(1000,20000),
            'transactionTotalGSTAmount' => Factory::create()->numberBetween(10, 150),
            'issuedStatus' => 'Issued',
            'customsStatus' => 'Approved',
            'refundStatus' => ''
        ];
    }

    public function generateMockGetTransactionStatusByDocIDResponse(): array
    {
        return [
            'docId' => Factory::create()->randomNumber(),
            'status' => [
                'status' => 'Issue',
                'issuedStatus' => 'Issued'
            ],
            'transactionUpdatedDatetime'=> Factory::create()->dateTime()->format('c')

        ];
    }

    public function generateMockInitiateRefundByDtroId(): array
    {
        return [
            'paymentResponse' => [
                'responseCode' => 0,
                'responseDescription' => 'Refund initiation completed successfully.'
            ]
        ];
    }

    public function generateMockRefundExecuted(): array
    {
        return [
          'paymentResponse' => [
              'responseCode' => 3,
              'responseDescription' => 'Refund-Executed completed successfully.'
          ]
        ];
    }

    public function generateMockRemoveRefundInitiatedFromDtro()
    {
        return [
            'paymentResponse' => [
                'responseCode' => 1,
                'responseDescription' => 'Refund-Initiated removal completed successfully.'
            ]
        ];
    }
}

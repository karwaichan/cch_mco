<?php

	namespace App\Logging;

	use Aws\CloudWatchLogs\CloudWatchLogsClient;
	use Maxbanton\Cwh\Handler\CloudWatch;
	use Monolog\Formatter\LineFormatter;
	use Monolog\Logger;

	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 21/2/2019
	 * Time: 2:26 PM
	 */
	class CloudWatchLoggerFactory
	{
		public function __invoke()
		{
			$format = "%channel%.%level_name%: %message% %context% %extra%\n";

			$sdkParams = config('logging.channels.cloudwatch.sdk');

			// Instantiate AWS SDK CloudWatch Logs Client
			$client = new CloudWatchLogsClient($sdkParams);

			// Log group name, will be created if none
			$groupName = env('CLOUDWATCH_LOG_GROUP', 'ETRS2.0');

			// Log stream name, will be created if none
			$streamName = env('CLOUDWATCH_STREAM_NAME', 'CCH2');

			// Days to keep logs, 14 by default. Set to `null` to allow indefinite retention.
			$retentionDays = (int)env('CLOUDWATCH_LOG_RETENTION', 30);

			// Instantiate handler (tags are optional)
			$handler = new CloudWatch($client, $groupName, $streamName, $retentionDays, 10000, ['tag' => 'cch2']);
			$handler->setFormatter(new LineFormatter($format, null, false, true));
			// Create a log channel
			$log = new Logger('name');
			// Set handler
			$log->pushHandler($handler);

			// Add records to the log
			return $log;
		}
	}
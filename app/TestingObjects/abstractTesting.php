<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 11/10/2018
	 * Time: 5:34 PM
	 */

	namespace App\TestingObjects;

	use Faker\Factory;

	abstract class abstractTesting
	{
		protected $fake;

		public function __construct()
		{
			$this->fake = Factory::create();
		}


		/**
		 * Get receipt number from storage\testing\apiParameter.txt
		 * Use the receipt number directly because is incremented
		 */
		protected function receiptNumber(): string
		{
			//get file content
			$fileContent = file_get_contents(\dirname(__DIR__, 2) . '/storage/testing/apiParameters.txt');
			//turn it into array and get receipt number
			$runningNumber = json_decode($fileContent, true)['receiptNumber'];
			//increment by 1 and save to file
			$receiptNumber = 'TOU_test' . $runningNumber;
			file_put_contents(\dirname(__DIR__, 2) . '/storage/testing/apiParameters.txt', json_encode([
																										   'receiptNumber' => ++$runningNumber
																									   ]));

			//return incremented number
			return $receiptNumber;
		}

		/**
		 * Take doc_id from etrs__etrs id BETWEEN 2003 and 3000
		 * @return string|null
		 */
		protected function documentId(): ?string
		{
			$documentIdArray = json_decode(file_get_contents(\dirname(__DIR__, 2) . '/storage/testing/documentId.txt'));
			$lastElement     = array_pop($documentIdArray);

			if (empty($lastElement)) {
				dd('Used up document id. Please pump in another 1000');
			}

			file_put_contents(\dirname(__DIR__, 2) . '/storage/testing/documentId.txt', json_encode($documentIdArray));

			return $lastElement;
		}
	}
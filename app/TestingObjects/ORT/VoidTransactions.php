<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 27/2/2019
	 * Time: 1:37 PM
	 */

	namespace App\TestingObjects\ORT;


	use App\TestingObjects\abstractTesting;

	class VoidTransactions extends abstractTesting
	{
		/**
		 * Case 6:
		 * @return array
		 */
		public function make_success_void_transaction(): array
		{
			return [
				'docId' => 	'20062110000000041734'
			];
		}

		/**
		 * Case 7 :
		 * @return array
		 */
		public function make_fail_void_transaction_not_in_database(): array
		{
			return [
				'docId' => '20062110000000039373'
			];
		}

		/**
		 * Case 9 :
		 * @return array
		 */
		public function make_fail_void_approved_transaction(): array
		{
			return [
				'docId' => '20062100066000015400'
			];
		}
	}
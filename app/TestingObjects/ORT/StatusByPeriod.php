<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 25/2/2019
	 * Time: 3:58 PM
	 */

	namespace App\TestingObjects\ORT;


	use App\TestingObjects\abstractTesting;

	class StatusByPeriod extends abstractTesting
	{
		/**
		 * Case 12:
		 * @return array
		 */
		public function make_transaction_with_invalid_datetime(): array
		{
			return [
				'DateTime' => '2018-04-44T15.23.25'
			];
		}

		/**
		 * Case 13:
		 * @return array
		 */
		public function make_transaction_with_valid_datetime(): array
		{
			return [
				'DateTime' => '2019-02-25 23:59:59'
			];
		}
	}
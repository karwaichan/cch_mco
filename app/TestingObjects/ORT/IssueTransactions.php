<?php

	namespace App\TestingObjects\ORT;

	use App\TestingObjects\abstractTesting;
	use Illuminate\Support\Carbon;
	use Illuminate\Support\Facades\Config;

	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 25/2/2019
	 * Time: 1:49 PM
	 */
	class IssueTransactions extends abstractTesting
	{
		/**
		 * Case 1
		 * @return array
		 */
		public function make_failed_consolidate_4_receipts_equal_100(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				'issueDateTime'           => '2019-02-25T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-25T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 20.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-25T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 25.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-25T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 40.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-25T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 15.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
				]
			];
		}

		/**
		 * Case 2
		 */
		public function make_3_out_of_4_receipts_more_than_100(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2019-02-25T08:37:17',
				'provisionalRefundAmount' => 6.00,
				'serviceFeeAmount'        => 2.18,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-24T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 15.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-24T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 10.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-24T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 80.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-25T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 20.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 3:
		 * @return array
		 */
		public function make_all_mandatory_fields(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				'issueDateTime'           => '2019-02-25T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-25T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 120.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 4:
		 * @return array
		 */
		public function make_transaction_without_refund_rule(): array
		{
			return [
				'docId'                   => $this->documentId(),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2019-02-25T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-25T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 5:
		 * @return array
		 */
		public function make_transaction_with_amount_less_than_minimum_eligible_amount(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				'issueDateTime'           => '2019-02-25T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2019-02-25T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 89.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Additional Scenario from NCS
		 * @return array
		 */
		public function make_additional_scenario(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				'issueDateTime'           => Carbon::now()->format('Y-m-d\TH:i:s'),
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 2.20,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => 'ORTTRG088',
					'nationality'    => 'AUS',
					'dateOfBirth'    => '1988-01-01'
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => 'ORT-88--Shop1-R1',
						'receiptDate'       => '2019-01-01T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 110.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Dept Store',
					]
				]
			];

		}
	}
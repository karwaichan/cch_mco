<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 25/2/2019
	 * Time: 5:10 PM
	 */

	namespace App\TestingObjects\ORT;


	use App\TestingObjects\abstractTesting;

	class StatusByDocId extends abstractTesting
	{

		/**
		 * Case 14:
		 * @return array
		 */
		public function make_transaction_not_in_database(): array
		{
			return [
				'docId' => '20062110000000039373'
			];
		}

		/**
		 * Case 15:
		 * @return array
		 */
		public function make_retrieve_transaction(): array
		{
			return [
				'docId' => '20062110000000041734'
			];
		}
	}
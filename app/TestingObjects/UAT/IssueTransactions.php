<?php

	namespace App\TestingObjects\UAT;


	use App\TestingObjects\abstractTesting;
	use Illuminate\Support\Facades\Config;


	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 9/10/2018
	 * Time: 12:31 PM
	 */
	final class IssueTransactions extends abstractTesting
	{
		/**
		 * Case 1
		 * fail_consolidation_3_receipts_less_than_100
		 * @return array
		 */
		public function make_receipts_less_than_100(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-12-19T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 20.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 25.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 40.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 15.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
				]
			];
		}

		/**
		 * Case 2:
		 * success_consolidation_3_receipts_more_than_100
		 * @return array
		 */
		public function make_3_receipts_more_than_100(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-12-19T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.87,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 15.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 10.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 80.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 3:
		 * failed_consolidate_3_receipts_more_than_100_same_day_and_1_receipt_different_day
		 */
		public function make_3_receipts_more_than_100_same_day_and_1_receipt_different_day(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-12-19T08:37:17',
				'provisionalRefundAmount' => 11.00,
				'serviceFeeAmount'        => 3.72,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 15.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 10.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 80.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-18T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 120.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 4:
		 * success_consolidate_4_receipts_more_than_100_same_day_and_2_receipt_more_than_100_different_day
		 */
		public function make_consolidate_4_receipts_more_than_100_same_day_and_2_receipt_more_than_100_different_day(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-12-19T08:37:17',
				'provisionalRefundAmount' => 11.00,
				'serviceFeeAmount'        => 3.72,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 15.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 10.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 80.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 10.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-18T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 70.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-18T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 40.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 5:
		 * success_issued_with_3_receipts_more_than_100_and_1_receipt_less_than_100
		 */

		public function make_3_receipts_more_than_100_and_1_receipt_less_than_100(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-12-20T08:37:17',
				'provisionalRefundAmount' => 28.00,
				'serviceFeeAmount'        => 7.33,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 150.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 110.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 200.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 80.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 6:
		 * fail_create_transaction_without_tourist_passport_number
		 */
		public function make_transaction_without_tourist_passport_number(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-12-20T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => '',
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 7:
		 * fail_create_transaction_with_invalid_document_id
		 */

		public function make_transaction_with_invalid_document_id(): array
		{
			return [
				'docId'                   => 'aaaaaaaa',
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-12-20T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 8
		 * fail_create_transaction_with_existing_doc_id
		 */

		public function make_transaction_with_existing_doc_id(): array
		{
			return [
				'docId'                   => '20062110000000042856',
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-12-20T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => '1',
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 9
		 * success_transaction_with_all_mandatory_fields
		 */

		public function make_transaction_with_all_mandatory_fields(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-12-20T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 10
		 * success_create_transaction_with_2_receipts_with_different_dates
		 */

		public function make_transaction_with_2_receipts_with_different_dates(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-12-20T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-17T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					],
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-12-18T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 7:
		 *
		 * make_success_with_all_mandatory_field
		 */
		public function make_success_with_all_mandatory_field(): array
		{
			return [
				'docId'                   => '20062610000000043115',
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-11-14T08:37:17',
				'provisionalRefundAmount' => 56.00,
				'serviceFeeAmount'        => 9.42,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-11-14T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 1000.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 8
		 * fail_create_transaction_without_doc_id
		 */
		public function make_transaction_without_doc_id(): array
		{
			return [
				'docId'                   => '',
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 9:
		 * fail_create_transaction_without_refund_rule
		 */
		public function make_transaction_without_refund_rule(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => '',
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 10
		 * fail_create_transaction_without_shop_id
		 */
		public function make_transaction_without_shop_id(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => '',
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 11
		 * fail_create_transaction_without_shop_name
		 */

		public function make_transaction_without_shop_name(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => '',
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 12:
		 *
		 * fail_create_transaction_without_shop_postcode
		 */

		public function make_transaction_without_shop_postcode(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 13:
		 * fail_create_transaction_without_gst_number
		 */

		public function make_transaction_without_gst_number(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => '',
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 14:
		 *
		 * fail_create_transaction_without_shop_activation_date
		 */

		public function make_transaction_without_shop_activation_date(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => ''
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 16:
		 * fail_create_transaction_without_tourist_nationality
		 * @return array
		 */
		public function make_transaction_without_tourist_nationality(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => '',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 17:
		 *
		 * fail_create_transaction_without_issue_date
		 * @return array
		 */
		public function make_transaction_without_issue_date(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 18:
		 * fail_create_transaction_without_provisional_refund_amount
		 * @return array
		 */
		public function make_transaction_without_provisional_refund_amount(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => '',
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 19:
		 * fail_create_transaction_without_service_fee_amount
		 */

		public function make_transaction_without_service_fee_amount(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => '',
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 20:
		 * fail_create_transaction_without_receipt_number
		 */
		public function make_transaction_without_receipt_number(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => '',
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 21
		 * fail_create_transaction_without_receipt_date_time
		 */

		public function make_transaction_without_receipt_date_time(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 22
		 * fail_create_transaction_without_gst_rate
		 */

		public function make_transaction_without_gst_rate(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => '',
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 23
		 * fail_create_transaction_without_item_gross_amount
		 */

		public function make_transaction_without_item_gross_amount(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => '',
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 24:
		 * fail_create_transaction_without_item_category
		 */

		public function make_transaction_without_item_category(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => '',
					]
				]
			];
		}

		/**
		 * Case 26
		 * fail_create_transaction_with_invalid_retailer_gst_register_number
		 */

		public function make_transaction_with_invalid_retailer_gst_register_number(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'asda',
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 27
		 * fail_create_transaction_with_invalid_tourist_date_of_birth
		 */

		public function make_transaction_with_invalid_tourist_date_of_birth(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => '2018-14-04'
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}

		/**
		 * Case 28:
		 * fail_create_transaction_with_invalid_purchase_item_quantity
		 */

		public function make_transaction_with_invalid_purchase_item_quantity(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 100.00,
						'itemQuantity'      => 'abcd',
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}



		/**
		 * Case 31
		 * success_transaction_with_amount_less_than_minimum_eligible
		 */

		public function make_transaction_with_amount_less_than_minimum_eligible(): array
		{
			return [
				'docId'                   => $this->documentId(),
				'refundRule'              => Config::get('constants.REFUND_RULE'),
				//				'issueDateTime'           => $this->fake->dateTimeBetween('-1 months', 'now', 'Asia/Singapore')->format('Y-m-d\TH:i:s'),
				'issueDateTime'           => '2018-09-07T08:37:17',
				'provisionalRefundAmount' => 5.00,
				'serviceFeeAmount'        => 1.54,
				'shop'                    => [
					'id'                    => $this->fake->randomNumber(8),
					'name1'                 => $this->fake->company(),
					'name2'                 => $this->fake->companySuffix(),
					'address1'              => $this->fake->streetName(),
					'address2'              => $this->fake->streetAddress(),
					'postalCode'            => '32150',
					'gstRegistrationNumber' => 'G' . $this->fake->randomNumber(9),
					'activationDate'        => $this->fake->date()
				],
				'traveller'               => [
					'passportNumber' => Config::get('constants.PASSPORT_NUMBER'),
					'nationality'    => 'JPN',
					'dateOfBirth'    => $this->fake->dateTimeBetween('1960-01-01 00:00:00', '2001-12-31 23:59:59')->format('Y-m-d')
				],
				'purchaseDetails'         => [
					[
						'receiptNumber'     => $this->receiptNumber(),
						'receiptDate'       => '2018-09-07T08:37:17',
						'gstRate'           => Config::get('constants.GST_RATE'),
						'description'       => 'Description',
						'detailDescription' => 'Detail Description',
						'itemGrossAmount'   => 89.00,
						'itemQuantity'      => 1,
						'goodsCategory'     => 'Fashion & Clothing',
					]
				]
			];
		}


	}
<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 11/10/2018
	 * Time: 5:33 PM
	 */

	namespace App\TestingObjects\UAT;


	use App\TestingObjects\abstractTesting;
	use Illuminate\Support\Carbon;

	class StatusByPeriod extends abstractTesting
	{

		/**
		 * Case 20
		 * fail_get_transaction_status_without_date_time
		 */

		public function make_transaction_status_without_date_time(): array
		{
			return [
				'DateTime' => ''
			];
		}

		/**
		 * Case 21
		 * fail_get_transaction_with_future_date_time
		 */

		public function make_transaction_with_future_date_time(): array
		{
			return [
				'DateTime' => '2019-10-12 15:23:25'
			];
		}

		/**
		 * Case22
		 * success_get_transaction_with_valid_date_time
		 */

		public function make_transaction_with_valid_date_time(): array
		{
			return [
				'DateTime' => '2018-12-20 00:00:00'
			];
		}

		/**
		 * Case 47
		 *success_get_transaction_status
		 */

		public function make_get_transaction_status(): array
		{
			return [
				'DateTime' => Carbon::now()->toDateTimeString()
			];
		}

		/**
		 * Case 48
		 *fail_get_transcation_status_on_no_transaction
		 */

		public function make_transcation_status_on_no_transaction(): array
		{
			return [
				'DateTime' => '2018-07-08 14:43:33'
			];
		}




		/**
		 * Case 50
		 * fail_get_transaction_with_invalid_date_time
		 */

		public function make_transaction_with_invalid_date_time(): array
		{
			return [
				'DateTime' => '2018-10-44 15:23:25'
			];
		}

		/**
		 * Case 52
		 * fail_get_transaction_with_3_months_earlier_date_time
		 */

		public function make_transaction_with_3_months_earlier_date_time(): array
		{
			return [
				'DateTime' => '2018-06-11 15:23:25'
			];
		}

		/**
		 * Case 53
		 * fail_get_transaction_for_date_time_with_no_transaction
		 */

		public function make_transaction_for_date_time_with_no_transaction(): array
		{
			return [
				'DateTime' => '2018-10-01 00:00:00'
			];
		}



	}
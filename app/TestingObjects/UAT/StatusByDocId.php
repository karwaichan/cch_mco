<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 12/10/2018
	 * Time: 9:15 AM
	 */

	namespace App\TestingObjects\UAT;


	use App\TestingObjects\abstractTesting;

	class StatusByDocId extends abstractTesting
	{
		/**
		 * Case 23
		 * success_get_transaction_with_valid_doc_id
		 */

		public function make_transaction_with_valid_doc_id(): array
		{
			return [
				'docId' => '20062100066000019683'
			];
		}

		/**
		 * Case 24
		 * fail_get_transaction_with_invalid_doc_id
		 */

		public function make_transaction_with_invalid_doc_id(): array
		{
			return [
				'docId' => 'zzzzzz'
			];
		}

		/**
		 * Case 59
		 * success_get_transaction_with_exist_doc_id
		 */

		public function make_transaction_with_exist_doc_id(): array
		{
			return [
				'docId' => '20062100067000020952'
			];
		}

		/**
		 * Case 56
		 * fail_get_transaction_with_blank_doc_id
		 */

		public function make2_transaction_with_blank_doc_id(): array
		{
			return [
				'docId' => ''
			];
		}

		/**
		 * Case 58
		 * fail_get_transaction_with_non_exist_doc_id
		 */

		public function make_transaction_with_non_exist_doc_id(): array
		{
			return [
				'docId' => '20062110000000011168'
			];
		}

	}
<?php

	namespace App\Validation;

	use App\Exceptions\EtrsValidationException;
	use App\ValueObjects\EtrsTransaction\Rules;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Log;
	use Illuminate\Http\JsonResponse;
	use Validator;

	/**
	 * Class EtrsValidation
	 * @package App\Validation
	 */
	class EtrsValidation
	{
		/**
		 * @param array $rules
		 */
		public function validate(array $rules): void
		{
			if (array_key_exists('purchaseDetails', request())) {
				$this->purchaseDetail(request('purchaseDetails'));
			}
			$validator = Validator::make(request()->toArray(), $rules);

			if ($validator->fails()) {
				throw new EtrsValidationException($validator);
			}
		}

		/**
		 * Check for purchase details.
		 * 1. Check newTransactions' purchase details
		 * 2. Check after @method newTransaction
		 */
		private function purchaseDetail($purchaseDetails): ?JsonResponse
		{
			foreach ($purchaseDetails as $purchaseDetail) {
				$purchaseDetailValidator = Validator::make($purchaseDetail, Rules::purchaseDetail());

				if ($purchaseDetailValidator->fails()) {
					throw new EtrsValidationException($purchaseDetailValidator);
				}
				unset($purchaseDetailValidator);
			}
		}
	}
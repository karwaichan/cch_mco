<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 26/9/2019
	 * Time: 1:38 PM
	 */

	namespace App\Exceptions;


	use Carbon\Carbon;
	use Exception;
	use Illuminate\Support\Facades\Log;
	use Illuminate\Support\Facades\Storage;

	class GeneralException extends Exception
	{
		private $generalException;
		private $requestPath;
		private $returnMessage;

		public function __construct($exception)
		{
			parent::__construct();
			$this->generalException = $exception;
			$this->requestPath = request()->path();
			$this->returnMessage = $exception->getResponse()->getBody()->getContents();
		}

		public function report() {

			if(array_key_exists('docId', request()->all())){
				Log::error(request()->path() . '_' . request()->get('docId'), $this->formatMessage());
			}

			if(array_key_exists('passportNumber', request()->all())) {
				Log::error(request()->path() . '_' . request()->get('passportNumber'), $this->formatMessage());
			}

			if(array_key_exists('travelerOverride', request()->all())){
				Log::error(request()->path() . '_' . request()->get('travelerOverride')['passportNumber'], $this->formatMessage());
			}

			$this->saveToFile();

		}

		public function render()
		{
			return $this->formatMessage();
		}

		private function formatMessage()
		{
			switch($this->requestPath) {
				case ($this->requestPath === 'api/search' || $this->requestPath === 'api/stamp'):
					return [
						'error' => true,
						'message' => json_decode($this->returnMessage, true)
					];
					break;
				case ($this->requestPath ==='api/void' || $this->requestPath ==='api/etrs'):
					$error = ['error' => true];
					return array_merge($error,json_decode($this->returnMessage, true));
					break;
				case ($this->requestPath === 'api/statusById' || $this->requestPath === 'api/statusByPeriod'):
					return [
						'error' => true,
						'response' => [json_decode($this->returnMessage, true)]
					];
					break;
				default:
					return [
						'error' => true,
						'message' => $this->returnMessage
					];
			}
		}

		private function saveToFile()
		{
			$s3 = Storage::disk('s3');

			$s3File = '/etrs2.0Logs/mcoLogs/' . request()->path() . '_' . Carbon::now()->format('Y_m_d_His') . '.txt';

			$s3->put($s3File, json_encode($this->generalException->getMessage()));
		}
	}

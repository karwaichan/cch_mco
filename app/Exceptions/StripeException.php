<?php

	namespace App\Exceptions;

    use Exception;

	class StripeException extends Exception
	{
        /**
         * @var Exception
         */
        private $exception;

        public function __construct(Exception $stripeException)
        {
            parent::__construct();
            $this->exception = $stripeException;
        }

        public function render()
        {
            return $this->formatMessage();
        }

        public function formatMessage()
        {
            return [
                'error' => true,
                'message' => $this->exception->getMessage()
            ];
        }

	}

<?php

	namespace App\Exceptions;

	use Illuminate\Contracts\Support\Responsable;
	use Illuminate\Http\JsonResponse;
	use Illuminate\Http\Request;
	use Illuminate\Support\Facades\Input;
	use Illuminate\Support\Facades\Log;
	use Illuminate\Support\Facades\Response;
	use Illuminate\Validation\ValidationException;

	class EtrsValidationException extends ValidationException implements Responsable
	{
		/**
		 * Create an HTTP response that represents the object.
		 *
		 * @param  \Illuminate\Http\Request $request
		 * @return \Illuminate\Http\JsonResponse
		 *
		 */
		public function toResponse($request): JsonResponse
		{
			$errorResponse = [
				'error'    => true,
				'response' => [
					'responseCode'        => 5000,
					'responseDescription' => $this->validator->errors()->getMessages()
				]
			];

			return response()->json($errorResponse);
		}
	}

<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Log;

class ConnectionException extends Exception
{
	public function report()
	{
		Log::error(request()->path(), $this->formatMessage());
    }

	public function render()
	{
		return $this->formatMessage();
    }

    private function formatMessage(){
		return [
			'error' => true,
			'message' => array_first(explode(PHP_EOL, $this->getMessage()))
		];
	}
}

<?php

namespace App\Exceptions;

use Exception;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class McoException extends Exception
{
	private $returnMessage;
	private $requestPath;
	private $clientException;

	public function __construct(ClientException $exception)
	{
		parent::__construct();
		$this->clientException = $exception;
		$this->requestPath = request()->path();
		$this->returnMessage = $exception->getResponse()->getBody()->getContents();
	}

	public function report()
	{
		#need to check this first, because NCS implemented throttling. Throttling they return HTTP code =429
		if($this->clientException->getCode() === 429) {
			Log::error($this->requestPath, $this->specialForThrottling());
		}
		else {

			if (array_key_exists('docId', request()->all())) {
				Log::error(request()->path() . '_' . request()->get('docId'), $this->formatMessage());
			}

			if (array_key_exists('passportNumber', request()->all())) {
				Log::error(request()->path() . '_' . request()->get('passportNumber'), $this->formatMessage());
			}

			if (array_key_exists('travelerOverride', request()->all())) {
				Log::error(request()->path() . '_' . request()->get('travelerOverride')['passportNumber'], $this->formatMessage());
			}

			if (array_key_exists('user_id', request()->all())) {
				Log::error(request()->path() . '_' . request('user_id'), $this->formatMessage());
			}
		}

		$this->saveToFile();
    }

    public function render()
	{
		if($this->clientException->getCode() === 429){
			return $this->specialForThrottling();
		}
		return $this->formatMessage();
	}

	/**
	 * Throttling demand a specific error message by NCS
	 * @return array
	 */
	private function specialForThrottling(): array
	{
		return [
			'error' => true,
			'message' => ['Please process your claim at eTRS kiosk.']
		];
	}

	private function formatMessage()
	{
		switch($this->requestPath) {
			case ($this->requestPath === 'api/search' || $this->requestPath === 'api/stamp' || $this->requestPath === 'api/alipay'):
				return [
					'error' => true,
					'message' => json_decode($this->returnMessage, true)
				];
				break;
			case ($this->requestPath ==='api/void' || $this->requestPath ==='api/etrs'):
				$error = ['error' => true];
				return array_merge($error,json_decode($this->returnMessage, true));
				break;
			case ($this->requestPath === 'api/statusById' || $this->requestPath === 'api/statusByPeriod' ):
				return [
					'error' => true,
					'response' => [json_decode($this->returnMessage, true)]
				   ];
				break;
			default:
				return [
					'error' => true,
					'message' => $this->returnMessage
				];
		}
	}

	private function saveToFile()
	{
		$s3 = Storage::disk('s3');

		$s3File = '/etrs2.0Logs/mcoLogs/' . request()->path() . '_' . Carbon::now()->format('Y_m_d_His') . '.txt';

		$s3->put($s3File, json_encode($this->clientException->getMessage()));
	}
}

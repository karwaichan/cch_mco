<?php

	namespace App\Http;

	use App\Interfaces\IRASRequestInterface;
	use Exception;
	use GuzzleHttp\Client;
	use Psr\Http\Message\ResponseInterface;

	/**
	 * Class ApiRequest
	 * @package App\Http
	 */
	class ApiRequest
	{
		/**
		 * @var Client
		 */
		private $client;

		public function __construct()
		{
			$this->client = new Client();
		}

		public function postEtrsRequest($requester)
		{
			try {
				return $this->getResponse($this->postRequest($requester));
			} catch (Exception $exception) {
				return $exception;
			}
		}

		public function getEtrsRequest($requester)
		{
			try {
				return $this->getResponse($this->getRequest($requester));
			} catch (Exception $exception) {
				return $exception;
			}
		}

		/**
		 * @param IRASRequestInterface $request
		 * @return mixed|ResponseInterface
		 * @internal param $apiPaths
		 * @internal param $headerOptions
		 */
		private function postRequest(IRASRequestInterface $request)
		{
			return $this->client->request('POST', $request->requestURL(), $request->headerOptions());
		}

		private function getRequest(IRASRequestInterface $request)
		{
			return $this->client->request('GET', $request->requestURL(), $request->headerOptions());
		}

		/**
		 * @param ResponseInterface $response
		 * @return string
		 */
		private function getResponse(ResponseInterface $response): string
		{
			return $response->getBody()->getContents();
		}

	}

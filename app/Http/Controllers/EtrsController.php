<?php

	namespace App\Http\Controllers;

	use App\Exceptions\IssueException;
	use App\Exceptions\McoException;
    use App\Services\CheckVoidTransactionService;
    use App\Services\RequestService;
	use App\Validation\EtrsValidation;
	use App\ValueObjects\AccessToken;
	use App\ValueObjects\EtrsTransaction\EtrsStatusBatch;
	use App\ValueObjects\EtrsTransaction\EtrsStatusById;
	use App\ValueObjects\EtrsTransaction\EtrsStatusByPeriod;
	use App\ValueObjects\EtrsTransaction\EtrsVoid;
	use App\ValueObjects\EtrsTransaction\newTransaction;
	use App\ValueObjects\EtrsTransaction\Rules;
	use App\ValueObjects\JWT;
	use Exception;
	use Illuminate\Http\JsonResponse;
	use Illuminate\Http\Request;
	use Illuminate\Support\Carbon;
	use Illuminate\Support\Facades\Log;
	use Illuminate\Support\Facades\Response;
	use Illuminate\Support\Facades\Storage;
	use Validator;

	/**
	 * Class EtrsController
	 *
	 * @package App\Http\Controllers
	 */
	class EtrsController extends Controller
	{
		CONST ISSUE = 'ISSUE';
		CONST VOID  = 'VOID';
		/**
		 * @var Request
		 */
		private $request;
		/**
		 * @var RequestService
		 */
		private $requestService;
		/**
		 * @var EtrsValidation
		 */
		private $etrsValidator;
        /**
         * @var CheckVoidTransactionService
         */
        private $checkVoidTransactionService;

        public function __construct(Request $request,
									RequestService $requestService,
									EtrsValidation $etrsValidation, CheckVoidTransactionService $checkVoidTransactionService)
		{
			$this->request        = $request;
			$this->requestService = $requestService;
			$this->etrsValidator  = $etrsValidation;
			$this->checkVoidTransactionService = $checkVoidTransactionService;
		}

		/**
		 * Create new eTRS transaction
		 *
		 * @return JsonResponse
		 *
		 * @OA\Post(
		 *     path = "/api/etrs",
		 *     tags={"etrs2.0"},
		 *     operationId = "issueTransaction",
		 *     summary ="Create new eTRS transaction",
		 *
		 *     @OA\Response(response="default", description= "Issue successful"),
		 *     @OA\Response(response = 500, description = "Exception error"),
		 *     @OA\RequestBody(ref="#/components/requestBodies/issue")
		 * )
		 */
		public function newTransaction()
		{
			$input = $this->request->all();

			//for CCH testing purpose
			$validationRules = env('VALIDATION') ? Rules::newTransaction() : [];
//			head($unprocessedReturn['response'])['responseCode']
			$output = $this->processEtrsRequest($validationRules, new newTransaction($this->request));

			return $output->getBody()->getContents();
		}

		/**
		 * Void existing eTRS doc ID
		 *
		 * @return JsonResponse
		 *
		 * @OA\Post(
		 *     path = "/api/void",
		 *     tags={"etrs2.0"},
		 *     operationId = "void",
		 *     summary = "Void existing eTRS doc ID",
		 *
		 *     @OA\Response(response = "default", description ="Voiding successful"),
		 *     @OA\Response(response = 500, description ="Exception error"),
		 *
		 *     @OA\RequestBody(ref="#/components/requestBodies/void")
		 *     ),
		 */
		public function voidTransaction()
		{
			$input = $this->request->all();

			//this to test CCH validation instead of our own
			$validationRules = env('VALIDATION') ? Rules::voidTransaction() : [];

			$output = $this->processEtrsRequest($validationRules, new EtrsVoid($this->request));

			return $output->getBody()->getContents();
		}

		/**
		 * Get at most 100 eTRS relative to counter from CCH
		 *
		 * @return JsonResponse
		 *
		 * @OA\Get(
		 *     path="/api/batchStatus",
		 *     tags={"etrs2.0"},
		 *     operationId = "batchStatus",
		 *     summary = "Get at most 100 eTRS relative to counter from CCH",
		 *
		 *     @OA\Response(
		 *        response = "default",
		 *        description = "Successfully called"
		 *     ),
		 *
		 * 		@OA\Response(
		 *            response = 500,
		 *            description = "Exception error"
		 *        ),
		 *)
		 */
		public function batchStatus()
		{
			$output = $this->processEtrsRequest([], new EtrsStatusBatch([]));

//			$this->saveToFile([], $output->getBody()->getContents(), '/statusBatch/');

			return $output->getBody()->getContents();
		}

		/**
		 * Get all doc Id status for the given period
		 *
		 * @return JsonResponse
		 *
		 * @OA\Post(
		 *     path = "/api/statusByPeriod",
		 *     tags={"etrs2.0"},
		 *     operationId = "statusByPeriod",
		 *     summary = "Get all eTRS doc Id for the given period",
		 *
		 *     @OA\Response(
		 *        response = "default",
		 *        description = "Successfully called"
		 *     ),
		 *
		 *     @OA\Response(
		 *        response = 500,
		 *     description = "Exception error"
		 *     ),
		 *
		 *     @OA\RequestBody(ref="#/components/requestBodies/statusByPeriod")
		 *
		 *     )
		 */
		public function batchStatusByPeriod()
		{
			$input = $this->request->all();

			$validationRules = env('VALIDATION') ? Rules::statusByPeriod() : [];

			$output = $this->processEtrsRequest($validationRules, new EtrsStatusByPeriod($this->request));

//			$this->saveToFile($input, $output->getBody()->getContents(), '/statusPeriod/');

			return $output->getBody()->getContents();
		}

		/**
		 * Get single doc Id status
		 *
		 * @return JsonResponse
		 *
		 * @OA\Post(
		 *    path="/api/statusById",
		 *     tags={"etrs2.0"},
		 *     operationId = "statusById",
		 *     summary = "Get all details regarding given document ID",
		 *
		 *     @OA\Response(
		 *            response = 500,
		 *            description ="Exceptions returned"
		 *     ),
		 *
		 *     @OA\Response(
		 *            response= 200,
		 *            description = "Details of document ID returned."
		 *     ),
		 *
		 *     @OA\RequestBody(ref="#/components/requestBodies/statusById"),
		 * )
		 */
		public function statusById()
		{
			$input = $this->request->all();

			$validationRules = env('VALIDATION') ? Rules::statusById() : [];

			$output = $this->processEtrsRequest($validationRules, new EtrsStatusById($this->request));

//			$this->saveToFile($input, $output->getContent(), '/statusById/' . $this->request->get('docId'), false);

			return $output->getBody()->getContents();
		}

		/**
		 * @param $validationRules
		 * @param $valueObjects
		 */
		private function processEtrsRequest($validationRules, $valueObjects)
		{
			//Validate request parameters
			$this->etrsValidator->validate($validationRules);

			return $this->requestService->sendRequest($valueObjects);
		}

		/**
		 * Save API input and output to logs files.
		 *
		 * @param      $input
		 * @param      $output
		 * @param      $fileName
		 * @param bool $includeDate
		 */
		private function saveToFile($input, $output, $fileName, $includeDate = true): void
		{
			$s3 = Storage::disk('s3');

			$s3File = !$includeDate ? '/etrs2.0Logs' . $fileName . '.txt' : '/etrs2.0Logs/' . $fileName . Carbon::now()->format('Y_m_d') . '.txt';

			if (!$s3->exists($s3File)) {
				$data = json_encode([array_merge($input, json_decode($output, true))]);
			}
			else {
				$files       = json_decode($s3->get($s3File), true);
				$currentData = array_merge($input, json_decode($output, true));
				array_push($files, $currentData);
				$data = json_encode($files);
			}

			$s3->put($s3File, $data);
		}

        public function checkVoidTransaction()
        {
            $this->checkVoidTransactionService->checkforNotTallyTransaction();
		}


		/**
		 * Only for create and void transaction.
		 * To keep track of records send to CCH
		 *
		 * @param $input
		 *
		 * @return array
		 */
		private function addCreatedDate($input): array
		{
			$createdDateArray = ['createdDate' => Carbon::now('Asia/Singapore')->toDateTimeString()];

			return array_merge($createdDateArray, $input);
		}

		public function token()
		{
			$output = $this->processEtrsRequest([], new AccessToken(new JWT()));
			return $output->getBody()->getContents();
		}
	}

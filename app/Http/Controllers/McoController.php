<?php

	namespace App\Http\Controllers;

	use App\Services\MCOCloudwatchLogService;
	use App\Services\RequestService;
	use App\ValueObjects\EtrsTransaction\MCOAlipayAccountConfirmation;
	use App\ValueObjects\EtrsTransaction\MCOGetAlipayAccount;
	use App\ValueObjects\EtrsTransaction\McoLanguage;
	use App\ValueObjects\EtrsTransaction\McoSearch;
	use App\ValueObjects\EtrsTransaction\McoStamp;
	use App\ValueObjects\JWT;
	use App\ValueObjects\MCOAcessToken;
    use GuzzleHttp\Psr7\Response as GuzzleResponse;
	use Illuminate\Support\Carbon;

	/**
	 * @property MCOCloudwatchLogService cloudwatchService
	 */
	class McoController extends Controller
	{
		private $requestService;

		public function __construct(RequestService $requestService, MCOCloudwatchLogService $MCOCloudwatchLogService)
		{
			$this->requestService = $requestService;
			$this->cloudwatchService = $MCOCloudwatchLogService;
		}

		public function stamp()
		{
			$result = $this->requestService->mcoRequest(new McoStamp());

			return $result->getBody()->getContents();

		}

		public function search()
		{
			$guzzleResponse = $this->requestService->mcoRequest(new McoSearch());

			return $this->returnContent($guzzleResponse);
		}

		/**
		 * @return string
		 */
		public function language(): string
		{
			$result = $this->requestService->mcoRequest(new McoLanguage());

			return $result->getBody()->getContents();
		}

		public function getAlipayAccount()
		{
			$guzzleResponse = $this->requestService->mcoRequest(new MCOGetAlipayAccount());

			return $this->returnContent($guzzleResponse);
		}

		public function alipayAccountConfirmation()
		{
			$guzzleResponse = $this->requestService->mcoRequest(new MCOAlipayAccountConfirmation());

			return $this->returnContent($guzzleResponse);
		}

		public function mcoToken()
		{
			$output = $this->requestService->mcoRequest(new MCOAcessToken(new JWT()));

			return $output->getBody()->getContents();
		}

		public function getMCOCloudwatchLogs()
		{
			$this->cloudwatchService->retrieveMCOCloudwatchLogs();
		}

		private function returnContent(GuzzleResponse $response)
		{
			return $response->getBody()->getContents();
		}
	}

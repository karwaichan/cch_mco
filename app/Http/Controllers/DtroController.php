<?php

namespace App\Http\Controllers;

use App\Exceptions\DtroException;
use App\Exceptions\GeneralException;
use App\Services\BinlistService;
use App\Services\EncryptCreditCardService;
use App\Services\MCFService;
use App\ValueObjects\MockData\DtroMockData;
use GuzzleHttp\Client;

class DtroController extends Controller
{
    const TRANSACTIONSTATUSBYDRTOIDURI = 'eventfeed/api/Dtro/GetTransactionByDtroId';
    const TRANSACTIONSTATUSBYIDURI = 'eventfeed/api/Dtro/GetTransactionStatusByDocId';
    const INITIATEREFUNDBYDTROID = 'refundcontrol/api/CchWrite/InitiateRefundByDtroId';
    const REFUNDEXECUTED = 'refundcontrol/api/CchWrite/RefundExecuted';
    const REMOVEREFUNDINITIATEDFROMDTROID = 'refundcontrol/api/CchWrite/RemoveInitiatedRefundByDtroId';
    /**
     * @var DtroMockData`
     */
    private $mock_data;
    /**
     * @var MCFService
     */
    private $mcfService;


    public function __construct(Client $client, MCFService $MCFService)
    {
        $this->client = $client;
        $this->mcfService = $MCFService;
    }

    /**
     * @throws DtroException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTransactionStatusByDtroID()
    {
        try{
            $result = $this->client->request('POST', config('constants.DTRO_BASE_URI'). self::TRANSACTIONSTATUSBYDRTOIDURI);
            return response($result->getBody()->getContents());
        }catch (\Exception $exception) {
            throw new DtroException($exception);
        }
    }

    /**
     * @throws DtroException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTransactionStatusByDocID()
    {
        try{
            $result = $this->client->request('POST', config('constants.DTRO_BASE_URI') . self::TRANSACTIONSTATUSBYIDURI, $this->headerOptions());
            return response($result->getBody()->getContents());
        }catch(\Exception $exception) {
            throw new DtroException($exception);
        }
    }

    /**
     * @throws DtroException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function initiateRefundByDtroId()
    {
        try{
            $result = $this->client->request('POST', config('constants.DTRO_BASE_URI') . self::INITIATEREFUNDBYDTROID, $this->headerOptions());

            return response($result->getBody()->getContents());
        }catch(\Exception $exception) {
            throw new DtroException($exception);
        }
    }

    /**
     * @throws DtroException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function refundExecuted()
    {
        try{
            $result = $this->client->request('POST', config('constants.DTRO_BASE_URI'). self::REFUNDEXECUTED,
                $this->headerOptions());
            return response($result->getBody()->getContents());
        }catch(\Exception $exception) {
            throw new DtroException($exception);
        }
    }

    /**
     * @throws DtroException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function removeRefundInitiatedFromDtro()
    {
        try{
            $result = $this->client->request('POST', config('constants.DTRO_BASE_URI').
                self::REMOVEREFUNDINITIATEDFROMDTROID, $this->headerOptions());
            dd($result->getBody()->getContents());
        }catch(\Exception $exception) {
            throw new DtroException($exception);
        }
    }

    public function encryptCreditCard(EncryptCreditCardService $encryptCreditCardService)
    {
        return response()->json($encryptCreditCardService->encryptCreditCard(request()->get('creditCard')));
    }

    /**
     * @throws DtroException
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function generateMCFFile()
    {
        try{
            $statusDtroId = $this->client->request('POST', config('constants.DTRO_BASE_URI') .
                self::TRANSACTIONSTATUSBYIDURI, $this->MCFOnlyHeaderOption());

            $statusResult = json_decode($statusDtroId->getBody()->getContents());

            $mcfFile = $this->mcfService->generate($statusResult);
        }catch(\Exception $exception) {
            throw new DtroException($exception);
        }
    }

    public function binlistCheck()
    {
        try{
            $cardNumber = request()->get('cardNumber');
            $binlist = new BinlistService($cardNumber);

            $binlist->check();
        }catch(\Exception $exception) {
            throw new DtroException($exception);
        }
    }

    private function MCFOnlyHeaderOption()
    {
        return [
            'base_uri' => config('constants.DTRO_BASE_URI'),
            'verify' => config('constants.CERTIFICATE'),
            'headers' => [
                'Content-Type' => 'application/json;charset=utf-8',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . request()->bearerToken(),
                'Cache-Control' => 'no-cache',
            ],
            'json' => ['docId' =>str_replace( '.', '', request()->get('docId'))]
        ];
    }

    private function headerOptions(): array
    {
        return [
            'base_uri' => config('constants.DTRO_BASE_URI'),
            'verify' => config('constants.CERTIFICATE'),
            'headers' => [
                'Content-Type' => 'application/json;charset=utf-8',
                'Accept' => 'application/json',
                'Authorization' => 'Bearer ' . request()->bearerToken(),
                'Cache-Control' => 'no-cache',
            ],
            'json' =>request()->all()
        ];
    }
}

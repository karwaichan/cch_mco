<?php

namespace App\Http\Controllers;

use App\ValueObjects\MockData\DtroMockData;
use Faker\Factory;
use Firebase\JWT\JWT;

class JWTController extends Controller
{
    private const TOKENURI = '/api/oauth/token';
    /**
     * @var DtroMockData
     */
    private $mockData;

    public function __construct(DtroMockData $mockData){
        $this->mockData = $mockData;
    }

    public function getBearerToken()
    {
        return response()->json($this->mockData->generateMockAccessTokenResponse());
        //return Http::withHeaders($this->headerOptions())->post(env('JWT_BASE_URI') . self::TOKENURI);
    }

    private function encodePayload(): string
    {
        $payload = [
            'iss' => env('JWT_ISS'),
            'aud' => env('JWT_AUD'),
            'exp' => env('JWT_EXP'),
            'iat' => env('JWT_IAT'),
        ];

        return JWT::encode($payload, env('JWT_KEY'), 'HS256');
    }

    private function headerOptions(): array
    {
        return [
            'base_uri' => env('JWT_BASE_URI'),
            'verify' => env('JWT_CERTIFICATE_PATH'),
            'timeout' => 5,
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Cache-Control' => 'no-cache'
            ],
            'form_params' => [
                'grant_type' => 'urn:ietf:params:oauth:grant-type:jwt-bearer',
                'assertion' => $this->encodePayload()
            ]
        ];
    }
}

<?php

	namespace App\Http\Controllers;

	use App\Services\BinlistService;
    use App\Services\StripeService;

    class StripeController extends Controller
	{
        /**
         * @var StripeService
         */
        private $stripeService;

        public function __construct()
        {
            $this->stripeService = new StripeService();
        }

        /**
         * Order statement(Order details/receipt) -> Profroma Invoice (temporary statement) -> Invoice (final statement)
         *
         * Payment intent = order statement
         * @throws \App\Exceptions\StripeException
         */
        public function testPI()
        {
            $paymentMethod = $this->stripeService->createPaymentMethod();
            $this->stripeService->createPaymentIntent($paymentMethod);
        }

        public function testCapture()
        {
            $this->stripeService->captureFundFromPaymentIntent();
        }

        public function testCancel()
        {
            $this->stripeService->cancelPreauth();
        }

        public function testBinList()
        {
            $binlistService = new BinlistService();
            return $binlistService->binLookup();
        }

	}

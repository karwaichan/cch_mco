<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class LogToCloudwatch
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
    	switch($request){
			case array_key_exists('passportNumber', $request->all()):
				Log::info($request->path() . '_' . $request->get('passportNumber'), $request->all());
				break;
			case array_key_exists('docId', $request->all()):
				Log::info($request->path() . '_' . $request->get('docId'), $request->all());
				break;
			case array_key_exists('travelerOverride', request()->all()):
				Log::info($request->path() . '_' . request()->get('travelerOverride')['passportNumber'], $request->all());
				break;
			case array_key_exists('user_id', request()->all()):
				Log::info($request->path() . '_' . request('user_id'), $request->all());
				break;
			default:
				Log::info($request->path(), $request->all());
		}

        return $next($request);
    }
}

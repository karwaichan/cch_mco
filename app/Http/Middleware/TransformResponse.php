<?php
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 27/9/2019
	 * Time: 11:00 AM
	 */

	namespace App\Http\Middleware;


	use App\Exceptions\McoException;
	use Closure;
	use Illuminate\Http\JsonResponse;
	use Illuminate\Support\Facades\Log;

	class TransformResponse
	{
		private $requestPath;

		public function __construct()
		{
			$this->requestPath = request()->path();
		}
		/**
		 * @param         $request
		 * @param Closure $next
		 *
		 * @return JsonResponse
		 */
		public function handle($request, Closure $next): JsonResponse
		{
			$response = $next($request);

			$transformedData = $this->transform($response);

			if($this->requestPath !== 'api/etrs' && $this->requestPath !== 'api/void') {
				if(array_key_exists('error', $transformedData->getData(true))){
					if($transformedData->getData(true)['error'] !== true){
						switch(true) {
							case array_key_exists('passportNumber', $request->all()):
								Log::notice($request->path() . '_' . $request->get('passportNumber'), $transformedData->getData(true));
								break;
							case array_key_exists('docId', $request->all()):
								Log::notice($request->path() . '_' . $request->get('docId'), $transformedData->getData(true));
								break;
							case array_key_exists('user_id', $request->all()):
								Log::notice($this->requestPath . '_' . request()->get('user_id'), $transformedData->getData(true));
								break;
							default:
								Log::notice($request->path(), $transformedData->getData(true));
						}
					}
				}
			}

			if($this->requestPath === 'api/stamp') {
				Log::notice($request->path() .'_'. request()->get('travelerOverride')['passportNumber'], $transformedData->getData(true));
			}

			if($this->requestPath === 'api/language') {
				Log::notice($request->path() . '_'. request()->get('passportNumber'), $transformedData->getData(true));
			}

			return $transformedData;
		}

		/**
		 * @param $response
		 *
		 * @return JsonResponse
		 */
		private function transform($response): JsonResponse
		{
			$jsonDecodedContents = json_decode($response->getContent(),true);

			if(!array_key_exists('error', $jsonDecodedContents)) {

				switch ($this->requestPath) {
					case ($this->requestPath === 'api/statusById' || $this->requestPath === 'api/statusByPeriod'):
						return response()->json(array_merge(['error' => false], $jsonDecodedContents));
						break;
					case ($this->requestPath === 'api/etrs' || $this->requestPath === 'api/void') :
						return $this->differentResponseForIssueAndVoid($jsonDecodedContents);
						break;
					case ($this->requestPath === 'api/stamp' || $this->requestPath === 'api/language'):
						//because GOT/GOM side does not take error=>true, it just take and throw whatever connector passed to them
						return response()->json($jsonDecodedContents);
						break;
					case ($this->requestPath === 'api/token' || $this->requestPath === 'api/mcoToken'):
						return response()->json(array_merge(['error' => false],$jsonDecodedContents));
						break;
					default:
						return response()->json([
													'error'   => false,
													'message' => $jsonDecodedContents
												]);
				}
			}

			return $response;
		}

		private function differentResponseForIssueAndVoid($content): JsonResponse
		{
			if(head($content['response'])['responseCode'] === '0100') {
				$response = array_merge(['error' => true], $content);

				Log::error($this->requestPath . '_' . request()->get('docId'), $response);

				return response()->json($response);
			}

			$successfulResponse = array_merge(['error' => false], $content);
			Log::notice($this->requestPath . '_' . request()->get('docId'), $successfulResponse);

			return response()->json($successfulResponse);
		}
	}

<?php

	namespace App\Interfaces;
	/**
	 * Created by KwChan ~ karwai@tourego.com.sg
	 * Date: 13/9/2018
	 * Time: 5:33 PM
	 */
	interface IRASRequestInterface
	{

		/**
		 * @return array
		 */
		public function headerOptions(): array;

		/**
		 * Iras API endpoint url
		 * @return mixed
		 */
		public function requestURL(): string;

	}
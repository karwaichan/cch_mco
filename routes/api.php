<?php

	use Illuminate\Http\Request;

	/*
	|--------------------------------------------------------------------------
	| API Routes
	|--------------------------------------------------------------------------
	|
	| Here is where you can register API routes for your application. These
	| routes are loaded by the RouteServiceProvider within a group which
	| is assigned the "api" middleware group. Enjoy building your API!
	|
	*/

	Route::middleware('auth:api')->get('/user', function (Request $request) {
		return $request->user();
	});

	Route::post('etrs', 'EtrsController@newTransaction')->middleware('transformResponse');
	Route::post('void', 'EtrsController@voidTransaction')->middleware('transformResponse');
	Route::get('batchStatus', 'EtrsController@batchStatus');
	Route::post('statusByPeriod', 'EtrsController@batchStatusByPeriod')->middleware('transformResponse');
	Route::post('statusById', 'EtrsController@statusById')->middleware('transformResponse');
	Route::post('token', 'EtrsController@token')->middleware('transformResponse');
	Route::post('stamp', 'McoController@stamp')->middleware('transformResponse');
	Route::post('search', 'McoController@search')->middleware('transformResponse');
	Route::post('language', 'McoController@language')->middleware('transformResponse');
	Route::post('mcoToken', 'McoController@mcoToken')->middleware('transformResponse');
	Route::post('alipay', 'McoController@getAlipayAccount')->middleware('transformResponse');
	Route::post('confirmAlipay', 'McoController@alipayAccountConfirmation')->middleware('transformResponse');
	Route::post('cloudwatchMCO', 'McoController@getMCOCloudwatchLogs');


    Route::post('dtrotoken', 'JwtController@getBearerToken');
    Route::post('getTransactionStatusByDtroId', 'DtroController@getTransactionStatusByDtroID');
    Route::post('getTransactionStatusByDocId', 'DtroController@getTransactionStatusByDocID');
    Route::post('initiateRefundByDtroId', 'DtroController@initiateRefundByDtroId');
    Route::post('refundExecuted', 'DtroController@refundExecuted');
    Route::post('removeRefundInitiatedFromDtro', 'DtroController@removeRefundInitiatedFromDtro');

    //Stripe

Route::post('stripe/PI', [\App\Http\Controllers\StripeController::class, 'testPI']);
Route::post('stripe/PM', [\App\Http\Controllers\StripeController::class, 'testPaymentMethod']);
Route::post('stripe/Capture', [\App\Http\Controllers\StripeController::class, 'testCapture']);
Route::post('stripe/cancel', [\App\Http\Controllers\StripeController::class, 'testCancel']);
Route::post('binlist', [\App\Http\Controllers\StripeController::class, 'testBinList']);

